## TCNATIVE 编译命令

### 国密TLS双证书

#### TASSL

##### 源码下载 

```sh
// 地址  https://github.com/jntass/TASSL-1.1.1
gh repo clone jntass/TASSL-1.1.1

```

##### 编译

```sh
// 配置
./config --prefix=/usr/local/tassl no-share 

// 编译
make && make install
```

#### APR 配置

##### 源码下载-WINDOWs

```sh
wget https://dlcdn.apache.org//apr/apr-1.7.4.tar.gz
```

##### 配置环境变量-WINDOWs

- 将解压后的bin目录添加到PATH下

##### 安装-LINUX

```sh
wget https://dlcdn.apache.org//apr/apr-1.7.4.tar.gz
./configure  --prefix=/usr/local/apr
make && make install
```

##### 安装-MAC

```sh
brew install apr
```

#### 配置TCNATIVE-BUILD

##### 配置pom（openssl-dynamic、libressl-static、boringssl-static、openssl-dynamic以及根目录下的pom文件）

- 配置`--with-ssl`
- 配置`--with-apr`
- 配置`CPPFLAGS`中include数据

```pom
<configureArgs>
  <configureArg>--with-ssl=/usr/local/ssl</configureArg>
  <configureArg>--with-apr=/usr/local/apr</configureArg>
  <configureArg>--with-static-libs</configureArg>
  <configureArg>--libdir=${project.build.directory}/native-build/target/lib</configureArg>
  <configureArg>${macOsxDeploymentTarget}</configureArg>
  <configureArg>CFLAGS=-O3 -Werror -fno-omit-frame-pointer -fvisibility=hidden -Wunused -Wnounused-but-set-variabl -Wno-unused-value</configureArg>
  <configureArg>CPPFLAGS=-DHAVE_OPENSSL -I$/usr/local/tassl/include/include</configureArg>
  <configureArg>LDFLAGS=-L${libresslHome}/ssl -L${libresslHome}/crypto -L${libresslHome}/tls -ltls -lssl -lcrypto</configureArg>
</configureArgs>
```

##### 编译命令

```shell
mvn clean install
```

- 第一次编译会失败，失败之后直接将`TASSL-1.1.1`复制到`openssl-static/target`下，并重命名为`openssl-1.1.1k`,继续执行`mvn install`