/*
Copyright (C) BABEC. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package io.netty.handler.ssl;

import java.util.*;

import static java.util.Arrays.asList;

public class GMSsl {

    public  static final String PROTOCOL_GMTLS_V1_1 = "GMTLSv1.1";

    public  static final String GMTLS_V1_1_KEY = "key";
    public  static final String GMTLS_V1_1_KEY_ENC = "key_enc";


    public static final List<String> GMTLSV11_CIPHERS = new ArrayList<>(
            asList("ECC-SM4-GCM-SM3", "ECDHE-SM4-GCM-SM3",
                    "ECC-SM4-SM3", "ECDHE-SM4-SM3"));

    private static boolean GMTLS_SUPPORT = false;

    public static boolean isGmtlsSupport() {
        return GMTLS_SUPPORT;
    }

    public static void setGmtlsSupport(boolean gmtlsSupport) {
        GMTLS_SUPPORT = gmtlsSupport;
    }


}
