/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.sdk.execption.ExceptionType;

public class ChainClientException extends SdkException {

    public ChainClientException(String message, ExceptionType exceptionType) {
        super(message, exceptionType);
    }

    public ChainClientException(String message) {
        super(message);
    }
}
