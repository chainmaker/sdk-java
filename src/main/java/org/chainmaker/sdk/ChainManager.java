/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.*;

import org.chainmaker.sdk.config.*;
import org.apache.commons.pool2.impl.BaseObjectPoolConfig;
import org.apache.commons.pool2.impl.EvictionPolicy;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.chainmaker.sdk.config.*;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.sync.TxResultDispatcher;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.chainmaker.sdk.utils.UtilsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;

public class ChainManager {

    private static final Logger logger = LoggerFactory.getLogger(ChainManager.class);

    static String OPENSSL_PROVIDER = "openSSL";
    static String TLS_NEGOTIATION = "TLS";

    // chains' map
    private Map<String, ChainClient> chains = new HashMap<>();
    // for singleton mode
    private static ChainManager chainManager = new ChainManager();

    private ChainManager() {
    }

    // singleton getInstance
    public static ChainManager getInstance() {
        return chainManager;
    }

    // get a chain client from chains
    public ChainClient getChainClient(String chainId) {
        return chains.get(chainId);
    }

    public synchronized ChainClient createChainClient(SdkConfig sdkConfig)
            throws ChainClientException, RpcServiceClientException, UtilsException, ChainMakerCryptoSuiteException {
        checkConfig(sdkConfig.getChainClient());
        String chainId = sdkConfig.getChainClient().getChainId();
        ChainClientConfig chainClientConfig = sdkConfig.getChainClient();
        dealChainClientConfig(chainClientConfig);

        User clientUser;
        if (chainClientConfig.getAuthType().equals(AuthType.PermissionedWithKey.getMsg()) ||
                chainClientConfig.getAuthType().equals(AuthType.Public.getMsg())) {
            if (chainClientConfig.getPkcs11() == null) {
                chainClientConfig.setPkcs11(new Pkcs11Config(false));
            }
            boolean pkcs11Enabled = chainClientConfig.getPkcs11().isEnabled();
            clientUser = new User(sdkConfig.getChainClient().getOrgId(),
                    chainClientConfig.getUserSignKeyBytes(),
                    chainClientConfig.getUserSignCrtBytes(),
                    chainClientConfig.getUserKeyBytes(), //pk、pwk支持tls
                    chainClientConfig.getUserCrtBytes(),
                    chainClientConfig.getUserEncKeyBytes(), // enc
                    chainClientConfig.getUserEncCrtBytes(),
                    chainClientConfig.getCrypto(), //使用证书中加密算法
                    pkcs11Enabled);
        } else { // Cert
            if (chainClientConfig.getPkcs11() == null) {
                chainClientConfig.setPkcs11(new Pkcs11Config(false));
            }
            boolean pkcs11Enabled = chainClientConfig.getPkcs11().isEnabled();
            clientUser = new User(sdkConfig.getChainClient().getOrgId(),
                    chainClientConfig.getUserSignKeyBytes(),
                    chainClientConfig.getUserSignCrtBytes(),
                    chainClientConfig.getUserKeyBytes(),
                    chainClientConfig.getUserCrtBytes(),
                    chainClientConfig.getUserEncKeyBytes(),
                    chainClientConfig.getUserEncCrtBytes(),pkcs11Enabled);
            if (sdkConfig.getChainClient().getAlias() != null && sdkConfig.getChainClient().getAlias().length() > 0) {
                clientUser.setAlias(sdkConfig.getChainClient().getAlias());
            }
        }

        clientUser.setAuthType(chainClientConfig.getAuthType());
        clientUser.setEnableTxResultDispatcher(chainClientConfig.getEnableTxResultDispatcher());

        List<Node> nodeList = new ArrayList<>();

        for (NodeConfig nodeConfig : sdkConfig.getChainClient().getNodes()) {
            List<byte[]> tlsCaCertList = new ArrayList<>();
            if (nodeConfig.getTrustRootBytes() == null && nodeConfig.getTrustRootPaths() != null) {
                for (String rootPath : nodeConfig.getTrustRootPaths()) {
                    List<String> filePathList = FileUtils.getFilesByPath(rootPath);
                    for (String filePath : filePathList) {
                        tlsCaCertList.add(FileUtils.getFileOrResourceFileBytes(filePath));
                    }
                }

                byte[][] tlsCaCerts = new byte[tlsCaCertList.size()][];
                tlsCaCertList.toArray(tlsCaCerts);
                nodeConfig.setTrustRootBytes(tlsCaCerts);
            }

            String url;

            if (nodeConfig.isEnableTls()) {
                url = "grpcs://" + nodeConfig.getNodeAddr();
            } else {
                url = "grpc://" + nodeConfig.getNodeAddr();
            }

            Node node = new Node();
            node.setTlsCertBytes(nodeConfig.getTrustRootBytes());
            if (nodeConfig.getChain_tls_host_name() != null && !nodeConfig.getChain_tls_host_name().isEmpty()) {
                node.setChainHostname(nodeConfig.getChainTtlsHostName());
            }
            node.setHostname(nodeConfig.getTlsHostName());
            node.setGrpcUrl(url);
            node.setSslProvider(OPENSSL_PROVIDER);
            node.setNegotiationType(TLS_NEGOTIATION);
//            node.setConnectCount(nodeConfig.getConnCnt());
            nodeList.add(node);
        }

        Node[] nodes = new Node[nodeList.size()];
        nodeList.toArray(nodes);
        if (chainClientConfig.getConnPool() == null) {
            chainClientConfig.setConnPool(new ConnPoolConfig());
        }
        if (chainClientConfig.getArchiveCenterQueryFirst() == null) {
            chainClientConfig.setArchiveCenterQueryFirst(false);
        }
        if (chainClientConfig.getEnableSendRequestSync() == null) {
            chainClientConfig.setEnableSendRequestSync(false);
        }
        return createChainClient(chainId, clientUser, nodes, chainClientConfig.getRpcClient().getMaxReceiveMessageSize(),
                chainClientConfig.getRetryInterval(), chainClientConfig.getRetryLimit(), chainClientConfig.getArchive(), chainClientConfig.getConnPool(),
                chainClientConfig.getArchiveCenterConfig(), chainClientConfig.getArchiveCenterQueryFirst(), chainClientConfig.getEnableSendRequestSync(),
                chainClientConfig.getCrypto(), chainClientConfig.getRpcClient().getProxyAddr());
    }

    // create a chain client by chain id, client user and nodes
    private ChainClient createChainClient(String chainId, User clientUser, Node[] nodes, int messageSize, int retryInterval, int retryLimit,
                                          ArchiveConfig archiveConfig, ConnPoolConfig connPoolConfig, ArchiveCenterConfig archiveCenterConfig,
                                          Boolean archiveCenterQueryFirst, Boolean enableSendRequestSync, CryptoConfig cryptoConfig, String proxyUrl)
            throws RpcServiceClientException, UtilsException, ChainClientException {
        ChainClient chainClient = chains.get(chainId);
        if (chainClient != null) {
            return chainClient;
        }

        GrpcClientFactory grpcClientFactory = new GrpcClientFactory(nodes, clientUser, messageSize);
        grpcClientFactory.setProxyUrl(proxyUrl);

        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        // 池中的最大连接数
        poolConfig.setMaxTotal(connPoolConfig.getMaxTotal());
        // 最少的空闲连接数
        poolConfig.setMinIdle(connPoolConfig.getMinIdle());
        // 最多的空闲连接数
        poolConfig.setMaxIdle(connPoolConfig.getMaxIdle());

        //空闲连接异常检测
        poolConfig.setTestWhileIdle(true);
        //当池中的资源耗尽时是否进行阻塞,设置false直接报错,true表示会一直等待，直到有可用资源
        poolConfig.setBlockWhenExhausted(connPoolConfig.isBlockWhenExhausted());
        //当设置阻塞时，等待时间
        poolConfig.setMaxWait(Duration.ofMillis(connPoolConfig.getMaxWaitMillis()));

        // 连接空闲的最小时间,达到此值后空闲连接可能会被移除,默认即为30分钟
        if (connPoolConfig.getMinEvictableIdleTime() != null) {
            poolConfig.setMinEvictableIdleTime(Duration.ofMillis(connPoolConfig.getMinEvictableIdleTime()));
        }
        // 连接空闲的最小时间,达到此值后并且当前空闲对象的数量大于最小空闲数量(minIdle)时，空闲连接可能会被移除,默认即为30分钟
        if (connPoolConfig.getSoftMinEvictableIdleTime() != null) {
            poolConfig.setSoftMinEvictableIdleTime(Duration.ofMillis(connPoolConfig.getSoftMinEvictableIdleTime()));
        }
        //此参数默认是-1，不会检查空闲连接。设置为正数后，才会检测
        if (connPoolConfig.getTimeBetweenEvictionRuns() != null) {
            poolConfig.setTimeBetweenEvictionRuns(Duration.ofMillis(connPoolConfig.getTimeBetweenEvictionRuns()));
        }
        poolConfig.setLifo(false);


        GenericObjectPool<RpcServiceClient> connPool = new GenericObjectPool<>(grpcClientFactory, poolConfig);
        for (int i = 0; i < poolConfig.getMinIdle(); i++) {
            try {
                connPool.addObject();
            } catch (Exception e) {
                logger.warn("create connpool object fail:%v", e);
            }
        }
        grpcClientFactory.setPool(connPool);
        chainClient = new ChainClient();
        chainClient.setChainId(chainId);
        chainClient.setRetryInterval(retryInterval);
        chainClient.setRetryLimit(retryLimit);
        chainClient.setClientUser(clientUser);
        chainClient.setConnectionPool(connPool);
        chainClient.setArchiveConfig(archiveConfig);
        chainClient.setArchiveCenterQueryFirst(archiveCenterQueryFirst);
        chainClient.setEnableSendRequestSync(enableSendRequestSync);
        chainClient.setEnableLowProfile(connPoolConfig.getEnableLowProfile());
        chainClient.setProxyUrl(proxyUrl);
        if (cryptoConfig != null && !Objects.equals(cryptoConfig.getHash(), "")) {
            chainClient.setHash(cryptoConfig.getHash());
        }
        if (retryInterval == 0) {
            chainClient.setRetryInterval(200);
        }
        if (retryLimit == 0) {
            chainClient.setRetryInterval(50);
        }
        if (clientUser.getEnableTxResultDispatcher() == null) {
            clientUser.setEnableTxResultDispatcher(false);
        }
        if (clientUser.getEnableTxResultDispatcher()) {
            chainClient.setDispatcher(new TxResultDispatcher(chainClient));
            chainClient.getDispatcher().start();
        }
        chainClient.setArchiveCenterConfig(archiveCenterConfig);

        chains.put(chainId, chainClient);

        if (chainClient.getClientUser().getAlias() != null && chainClient.getClientUser().getAlias().length() > 0) {
            try {
                chainClient.enableAlias();
            } catch (ChainMakerCryptoSuiteException | ChainClientException e) {
                throw new ChainClientException("enable Alias failed: " + e.getMessage());
            }
        }
        return chainClient;
    }

    private void dealChainClientConfig(ChainClientConfig chainClientConfig)
            throws UtilsException, ChainMakerCryptoSuiteException {

        String authType = chainClientConfig.getAuthType();
        if (authType.equals(AuthType.PermissionedWithKey.getMsg()) || authType.equals(AuthType.Public.getMsg())) {
            byte[] pemKey = chainClientConfig.getUserSignKeyBytes();
            if (pemKey == null && chainClientConfig.getUserSignKeyFilePath() != null) {
                chainClientConfig.setUserSignKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserSignKeyFilePath()));
            }


            byte[] userSignCrtBytes = chainClientConfig.getUserSignCrtBytes();
            if (userSignCrtBytes == null && chainClientConfig.getUserSignCrtFilePath() != null) {
                chainClientConfig.setUserSignCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserSignCrtFilePath()));
            }

            byte[] userKeyBytes = chainClientConfig.getUserKeyBytes();
            if (userKeyBytes == null && chainClientConfig.getUserKeyFilePath() != null) {
                chainClientConfig.setUserKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserKeyFilePath()));
            }

            byte[] userCrtBytes = chainClientConfig.getUserCrtBytes();
            if (userCrtBytes == null && chainClientConfig.getUserCrtFilePath() != null) {
                chainClientConfig.setUserCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserCrtFilePath()));
            }

            byte[] userEncKeyBytes = chainClientConfig.getUserEncKeyBytes();
            if (userEncKeyBytes == null && chainClientConfig.getUserEncKeyFilePath() != null) {
                chainClientConfig.setUserEncKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserEncKeyFilePath()));
            }

            byte[] userEncCrtBytes = chainClientConfig.getUserEncCrtBytes();
            if (userEncCrtBytes == null && chainClientConfig.getUserEncCrtFilePath() != null) {
                chainClientConfig.setUserEncCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserEncCrtFilePath()));
            }
        } else { //Cert
            chainClientConfig.setAuthType(AuthType.PermissionedWithCert.getMsg());
            byte[] userKeyBytes = chainClientConfig.getUserKeyBytes();

            if (userKeyBytes == null && chainClientConfig.getUserKeyFilePath() != null) {
                chainClientConfig.setUserKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserKeyFilePath()));
            }

            byte[] userCrtBytes = chainClientConfig.getUserCrtBytes();
            if (userCrtBytes == null && chainClientConfig.getUserCrtFilePath() != null) {
                chainClientConfig.setUserCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserCrtFilePath()));
            }

            byte[] userEncKeyBytes = chainClientConfig.getUserEncKeyBytes();

            if (userEncKeyBytes == null && chainClientConfig.getUserEncKeyFilePath() != null) {
                chainClientConfig.setUserEncKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserEncKeyFilePath()));
            }

            byte[] userEncCrtBytes = chainClientConfig.getUserEncCrtBytes();
            if (userEncCrtBytes == null && chainClientConfig.getUserEncCrtFilePath() != null) {
                chainClientConfig.setUserEncCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserEncCrtFilePath()));
            }

            byte[] userSignKeyBytes = chainClientConfig.getUserSignKeyBytes();
            if (userSignKeyBytes == null && chainClientConfig.getUserSignKeyFilePath() != null) {
                chainClientConfig.setUserSignKeyBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserSignKeyFilePath()));
            }

            byte[] userSignCrtBytes = chainClientConfig.getUserSignCrtBytes();
            if (userSignCrtBytes == null && chainClientConfig.getUserSignCrtFilePath() != null) {
                chainClientConfig.setUserSignCrtBytes(FileUtils.getFileOrResourceFileBytes(chainClientConfig.getUserSignCrtFilePath()));
            }
        }

    }

    private void checkConfig(ChainClientConfig chainClientConfig) throws ChainClientException {
        if (chainClientConfig == null) {
            logger.error("chainClientConfig is null, please check config");
            throw new ChainClientException("chainClientConfig is null");
        }

        checkNodeListConfig(chainClientConfig);
        checkUserConfig(chainClientConfig);
        checkChainConfig(chainClientConfig);
    }

    private void checkNodeListConfig(ChainClientConfig chainClientConfig) throws ChainClientException {

        NodeConfig[] nodeConfigs = chainClientConfig.getNodes();

        for (NodeConfig nodeConfig : nodeConfigs) {
//            if (nodeConfig.getConnCnt() <= 0 || nodeConfig.getConnCnt() > NodeConfig.maxConnCnt) {
//                throw new ChainClientException(String.format("node connection count should >0 && <=%d", NodeConfig.maxConnCnt));
//            }

            if (nodeConfig.isEnableTls()) {
                if (nodeConfig.getTrustRootBytes() == null && nodeConfig.getTrustRootPaths() == null) {
                    throw new ChainClientException("if node useTLS is open, should set caPaths or caCerts");
                }
            }

            if ("".equals(nodeConfig.getTlsHostName())) {
                throw new ChainClientException("if node useTLS is open, should set tls hostname");
            }
        }
    }

    private void checkUserConfig(ChainClientConfig chainClientConfig) throws ChainClientException {
        if ("".equals(chainClientConfig.getUserKeyFilePath()) && chainClientConfig.getUserKeyBytes() == null) {
            throw new ChainClientException("user key cannot be empty");
        }
        if ("".equals(chainClientConfig.getUserCrtFilePath()) && chainClientConfig.getUserCrtBytes() == null) {
            throw new ChainClientException("user cert cannot be empty");
        }
    }

    private void checkChainConfig(ChainClientConfig chainClientConfig) throws ChainClientException {
        if ("".equals(chainClientConfig.getChainId())) {
            throw new ChainClientException("chainId cannot be empty");
        }
    }
}
