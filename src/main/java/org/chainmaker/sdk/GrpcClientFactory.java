/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.DestroyMode;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.chainmaker.pb.api.RpcNodeGrpc;
import org.chainmaker.pb.config.ChainmakerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GrpcClientFactory extends BasePooledObjectFactory<RpcServiceClient> {

    private static final Logger logger = LoggerFactory.getLogger(GrpcClientFactory.class);

    private final User clientUser;
    private final int messageSize;

    private String proxyUrl;

    //待连接节点
    private List<Node> readNodes;
    // 维护节点 node_addr + _ + tls_host_name（"127.0.0.1:12301_chainmaker.org"）的连接数
    // 每个节点可以建立多个连接，新建连接时使用连接数较少的节点连接。
    private Map<String, AtomicInteger> nodeConnCountMap;
    // 客户端连接 ——> 地址 node_addr + _ + tls_host_name
    private Map<RpcServiceClient, String> connNodeMap;
    //业务可用性检测时的超时时间，默认10s.
    final long rpcCallTimeout = 10000;
    //对应的连接池
    GenericObjectPool<RpcServiceClient> connPool;

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public void setPool(GenericObjectPool<RpcServiceClient> connPool) {
        this.connPool = connPool;
    }

    /**
     * @param nodes       节点信息数组
     * @param clientUser  用户信息
     * @param messageSize grpc客户端最大接受容量(MB)
     */
    public GrpcClientFactory(Node[] nodes, User clientUser, int messageSize) {
        this.clientUser = clientUser;
        this.readNodes = Arrays.stream(nodes).collect(Collectors.toList());
        this.messageSize = messageSize;
        nodeConnCountMap = new ConcurrentHashMap<>(nodes.length);
        connNodeMap = new ConcurrentHashMap<>();
    }


    //动态添加一个加节点
    public void addNode(Node node) {
        logger.info("add node:{}", node.getUri());
        readNodes.add(node);
        logReadNode();
    }

    public void delNode(Node node) {
        logger.info("del node:{}", node.getUri());
        boolean removed = readNodes.remove(node);
        //当前已经有建立连接，并且存在与待连接节点中，找出该节点建立的连接
        Set<Map.Entry<RpcServiceClient, String>> keepConn = connNodeMap.entrySet();
        if (keepConn.size() > 0 && removed) {
            List<RpcServiceClient> delClients = new ArrayList<>();

            for (Map.Entry<RpcServiceClient, String> entry : keepConn) {
                if (entry.getValue().equals(node.getUri())) {
                    logger.debug("found node grpc url:{}", node.getUri());
                    RpcServiceClient delClient = entry.getKey();
                    delClients.add(delClient);
                }
            }

            for (RpcServiceClient delClient : delClients) {
                try {
                    //移除连接
                    this.connPool.invalidateObject(delClient, DestroyMode.NORMAL);
                    logger.debug("grpc client destroyed:{}", delClient);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            }
        }

        logReadNode();
    }


    @Override
    public RpcServiceClient create() throws Exception {
        //用户保存异常节点，用于当次连接时避开错误节点
        Set<String> errorNodes = new HashSet<>();
        //创建一个连接
        RpcServiceClient rpcServiceClient;
        try {
            rpcServiceClient = createRpcClient(errorNodes);
        } finally {
            //最后把errorNodes清理掉，下次错误节点可以继续重试
            errorNodes.clear();
        }
        logger.info("create a new  grpc client:{}....", rpcServiceClient);
        return rpcServiceClient;
    }

    private RpcServiceClient createRpcClient(Set<String> errorNodes) {
        //获取连接数最少的节点
        RpcServiceClient rpcServiceClient = null;
        Node node = null;
        //创建一个连接，直到成功，或者没有节点可以连后抛错
        while (rpcServiceClient == null) {
            node = getLeastConnNode(errorNodes);
            if (node == null) {
                logger.error("create chainClient error:no node can use");
                logger.info("errorNodes:" + errorNodes);
                logReadNode();
                throw new RuntimeException("create chainClient error:no node can use");
            }
            try {
                //创建连接后，必须确保连接已经是通了
                rpcServiceClient = RpcServiceClient.newServiceClient(node, clientUser, messageSize, proxyUrl);


                boolean b = buseCheck(rpcServiceClient);

                if (!b) {
                    logger.warn("===============创建连接失败,把节点加入异常节点{}", node.getUri());
                    //如果创建连接失败,把节点加入到异常节点中，下次获取时避开
                    errorNodes.add(node.getUri());
                    if (errorNodes.size() >= readNodes.size()) {
                        logger.warn("create chainClient error:no node can use");
                        throw new RuntimeException("create chainClient error:no node can use");
                    }
                    rpcServiceClient.getManagedChannel().shutdown();
                    rpcServiceClient = null;
                }
            } catch (Exception e) {
                if (rpcServiceClient != null) {
                    rpcServiceClient.getManagedChannel().shutdown();
                }
                logger.warn("===============创建连接失败{},把节点加入异常节点{}", e.getMessage(), node.getUri());
                logger.warn("===============创建连接失败===============", e);
                //如果创建连接失败,把节点加入到异常节点中，下次获取时避开
                errorNodes.add(node.getUri());
                if (errorNodes.size() >= readNodes.size()) {
                    logger.error("create chainClient error:no node can use", e);
                    throw new RuntimeException("create chainClient error:no node can use");
                }
            }

        }

        //维护rpc 和节点关系
        connNodeMap.put(rpcServiceClient, node.getUri());
        AtomicInteger count = nodeConnCountMap.putIfAbsent(node.getUri(), new AtomicInteger(0));
        if (count == null) {
            count = nodeConnCountMap.get(node.getUri());
        }
        //该节点连接数增加1
        count.incrementAndGet();
        logger.info("create a new  grpc client:{} {} current conn {}....", rpcServiceClient, node.getUri(), count.get());
        return rpcServiceClient;
    }

    //获取连接最少的节点
    private Node getLeastConnNode(Set<String> errorNodes) {
        Node leastNode = null;
        int least = Integer.MAX_VALUE;
        for (Node n : readNodes) {
            //避开错误节点
            if (errorNodes.contains(n.getUri())) {
                continue;
            }
            //找出连接数最小的节点，刚开始，节点连接数为0
            AtomicInteger nodeCount = nodeConnCountMap.getOrDefault(n.getUri(), new AtomicInteger(0));
            if (nodeCount.get() < least) {
                least = nodeCount.get();
                leastNode = n;
            }
        }
        if (leastNode != null) {
            logger.debug("getLeastConnNode grpc url:{}...", leastNode.getUri());
        } else {
            logger.debug("getLeastConnNode grpc is null");
        }
        return leastNode;
    }

    @Override
    public PooledObject<RpcServiceClient> wrap(RpcServiceClient client) {
        return new DefaultPooledObject<>(client);
    }

    @Override
    public void destroyObject(PooledObject<RpcServiceClient> p) throws Exception {
        long start = System.currentTimeMillis();
        RpcServiceClient rpcServiceClient = p.getObject();
        rpcServiceClient.getManagedChannel().shutdown();
        try {
            // try fix err: RuntimeException: ManagedChannel allocation site
            //              Make sure to call shutdown()/shutdownNow()and wait until awaitTermination() returns true.
            if (!rpcServiceClient.getManagedChannel().awaitTermination(3, TimeUnit.SECONDS)) {
                rpcServiceClient.getManagedChannel().shutdownNow();
            }
        } catch (Exception e) {
            logger.info("destroyObject exception :{}", e.getMessage());
        }
        //节点连接数减掉1
        String uri = connNodeMap.get(rpcServiceClient);
        nodeConnCountMap.get(uri).decrementAndGet();
        //移除连接
        connNodeMap.remove(rpcServiceClient);
        super.destroyObject(p);

        logger.info("destroyObject: {} cost: {}ms", rpcServiceClient, System.currentTimeMillis() - start);
    }

    // 连接关闭
    public void close(RpcServiceClient rpcServiceClient) {
        long start = System.currentTimeMillis();
        rpcServiceClient.getManagedChannel().shutdownNow();
        //节点连接数减掉1
        String uri = connNodeMap.get(rpcServiceClient);
        nodeConnCountMap.get(uri).decrementAndGet();
        //移除连接
        connNodeMap.remove(rpcServiceClient);
        logger.info("close: {} cost: {}ms", rpcServiceClient, System.currentTimeMillis() - start);
    }

    //检测连接可用性
    @Override
    public boolean validateObject(final PooledObject<RpcServiceClient> p) {
        RpcServiceClient rpcServiceClient = p.getObject();
        ManagedChannel managedChannel = rpcServiceClient.getManagedChannel();
        ConnectivityState connectivityState = managedChannel.getState(true);
        logger.info(" {} check validateObject......{}", managedChannel, connectivityState.name());
        //如果连接grpc通道是可用
        if (connectivityState.equals(ConnectivityState.READY) || connectivityState.equals(ConnectivityState.IDLE)) {
            return buseCheck(rpcServiceClient);
            //如果连接grpc通道不可用
        } else if (connectivityState.equals(ConnectivityState.SHUTDOWN)) {
            return false;
        } else if (connectivityState.equals(ConnectivityState.CONNECTING) || connectivityState.equals(ConnectivityState.TRANSIENT_FAILURE)) {
            return checkReady(managedChannel);
        }
        return true;
    }

    private boolean buseCheck(RpcServiceClient rpcServiceClient) {
        //就绪状态下，做一次业务检测
        ChainmakerServer.ChainMakerVersionRequest chainMakerVersionRequest = ChainmakerServer.ChainMakerVersionRequest.newBuilder().build();
        RpcNodeGrpc.RpcNodeFutureStub rpcNodeFutureStub = rpcServiceClient.getRpcNodeFutureStub();
        try {
            rpcNodeFutureStub.getChainMakerVersion(chainMakerVersionRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.warn("check invoke getVersion error one ", e);
            return false;
        }
        return true;
    }

    private boolean checkReady(ManagedChannel managedChannel) {
        int count = 100;
        while (count > 0 && !managedChannel.getState(true).equals(ConnectivityState.READY)) {
            try {
                TimeUnit.MILLISECONDS.sleep(20);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            count--;
        }
        //执行100次后，通道还未就绪，关闭通道
        if (managedChannel.getState(true).equals(ConnectivityState.READY)) {
            return true;
        }
        logger.info("managedChannel not ready......");
        return false;
    }

    /**
     * 关闭所有链接池
     */
    public void stopAll() {
        Set<RpcServiceClient> clients = connNodeMap.keySet();

        for (RpcServiceClient rpcServiceClient : clients) {
            try {
                // pool 会调用 destroyObject 实现，关闭各个通道
                connPool.invalidateObject(rpcServiceClient);
            } catch (Exception e) {
                logger.error("invalidate object err:{}", e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    private void logReadNode() {
        logger.info("now nodes:{}", readNodes.stream().map(Node::getUri).collect(Collectors.toList()));
    }
}
