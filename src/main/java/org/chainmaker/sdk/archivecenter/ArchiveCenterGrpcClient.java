/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import org.chainmaker.pb.archivecenter.ArchiveCenterServerGrpc;
import com.google.common.collect.ImmutableMap;
import com.zayk.util.encoders.Hex;
import io.grpc.ManagedChannel;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NegotiationType;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.StreamObserver;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;
import org.chainmaker.pb.archivecenter.Archivecenter;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.sdk.ChainClientException;
import org.chainmaker.sdk.config.ArchiveCenterConfig;
import org.chainmaker.sdk.execption.ExceptionType;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * 归档中心grpc实现
 */
public class ArchiveCenterGrpcClient implements ArchiveService {

    private ArchiveCenterServerGrpc.ArchiveCenterServerStub archiveCenterServerStub;

    private ArchiveCenterServerGrpc.ArchiveCenterServerFutureStub archiveCenterServerFutureStub;

    private ArchiveCenterConfig archiveCenterConfig;


    private static final Logger logger = LoggerFactory.getLogger(ArchiveCenterHttpClient.class);

    private static final Map<Class<?>, Class<?>> WRAPPERS_TO_PRIM = new ImmutableMap.Builder<Class<?>, Class<?>>().put(Boolean.class, boolean.class).put(Byte.class, byte.class).put(Character.class, char.class).put(Double.class, double.class).put(Float.class, float.class).put(Integer.class, int.class).put(Long.class, long.class).put(Short.class, short.class).put(Void.class, void.class).build();

    // 初始化grpcClient
    public ArchiveCenterGrpcClient(ArchiveCenterConfig archiveCenterConfig) throws ChainClientException {
        int maxSendSize = archiveCenterConfig.getMaxSendMsgSize() * 1024 * 1024;
        ManagedChannel channel = null;

        Properties nettyBuilderProperties = new Properties();
        nettyBuilderProperties.put("keepAliveTime", new Object[]{5L, TimeUnit.MINUTES});
        nettyBuilderProperties.put("keepAliveTimeout", new Object[]{8L, TimeUnit.SECONDS});
        nettyBuilderProperties.put("keepAliveWithoutCalls", new Object[]{true});

        try {
            // 开启tls
            if (archiveCenterConfig.getTlsEnable()) {
                NettyChannelBuilder nettyChannelBuilder = NettyChannelBuilder.forTarget(archiveCenterConfig.getRpcAddress());
                nettyChannelBuilder.maxInboundMessageSize(maxSendSize);
                nettyChannelBuilder.maxInboundMetadataSize(maxSendSize);
                byte[] certBytes = FileUtils.getFileBytes(archiveCenterConfig.getTls().getCertFile());
                Certificate certificate = CryptoUtils.parseCertificate(certBytes);
                X509Certificate[] clientCert = new X509Certificate[]{(X509Certificate) certificate};

                byte[] keyBytes = FileUtils.getFileBytes(archiveCenterConfig.getTls().getPrivKeyFile());
                PrivateKey privateKey = CryptoUtils.getPrivateKeyFromBytes(keyBytes);
                final AbstractMap.SimpleImmutableEntry<PrivateKey, X509Certificate[]> clientTLSProps =
                        new AbstractMap.SimpleImmutableEntry<>(privateKey, clientCert);

                clientCert = clientTLSProps.getValue();
                privateKey = clientTLSProps.getKey();
                List<byte[]> tlsCaCertList = new ArrayList<>();
                for (String rootPath : archiveCenterConfig.getTls().getTrustCaList()) {
                    List<String> filePathList = FileUtils.getFilesByPath(rootPath);
                    for (String filePath : filePathList) {
                        tlsCaCertList.add(FileUtils.getFileBytes(filePath));
                    }
                }
                byte[][] tlsCaCerts = new byte[tlsCaCertList.size()][];
                tlsCaCertList.toArray(tlsCaCerts);

                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                ArrayList<X509Certificate> x509CertificateList = new ArrayList<>();
                for (byte[] tlsCaCert : tlsCaCerts) {
                    X509Certificate x509Certificate = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(tlsCaCert));
                    x509CertificateList.add(x509Certificate);
                }
                SslContext sslContext = GrpcSslContexts.configure(SslContextBuilder.forClient(), SslProvider.OPENSSL)
                        .keyManager(privateKey, clientCert)
                        .trustManager(x509CertificateList)
                        .build();
                nettyChannelBuilder.sslContext(sslContext).negotiationType(NegotiationType.TLS).overrideAuthority(archiveCenterConfig.getTls().getServerName());
                addNettyBuilderProps(nettyChannelBuilder, nettyBuilderProperties);
                channel = nettyChannelBuilder.build();
            } else {
                // tls 关闭
                NettyChannelBuilder nettyChannelBuilder = NettyChannelBuilder.forTarget(archiveCenterConfig.getRpcAddress());
                nettyChannelBuilder.maxInboundMessageSize(maxSendSize);
                nettyChannelBuilder.maxInboundMetadataSize(maxSendSize);
                nettyChannelBuilder.usePlaintext();
                addNettyBuilderProps(nettyChannelBuilder, nettyBuilderProperties);
                channel = nettyChannelBuilder.build();
            }
            archiveCenterServerStub = ArchiveCenterServerGrpc.newStub(channel);
            archiveCenterServerFutureStub = ArchiveCenterServerGrpc.newFutureStub(channel);
            this.archiveCenterConfig = archiveCenterConfig;
        } catch (Exception e) {
            logger.error("create archive center grpc client fail, err:{}", e.getMessage());
            throw new ChainClientException(e.getMessage(), ExceptionType.CONNECT);
        }
    }

    public ArchiveCenterConfig getArchiveCenterConfig() {
        return archiveCenterConfig;
    }

    public void setArchiveCenterConfig(ArchiveCenterConfig archiveCenterConfig) {
        this.archiveCenterConfig = archiveCenterConfig;
    }

    @Override
    public ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long rpcCallTimeout) throws ChainClientException{
        Archivecenter.BlockByTxIdRequest blockByTxIdRequest = Archivecenter.BlockByTxIdRequest.newBuilder().setTxId(txId).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        ChainmakerTransaction.TransactionInfo.Builder transactionInfo = ChainmakerTransaction.TransactionInfo.newBuilder();
        try {
            Archivecenter.TransactionResp transactionResp = archiveCenterServerFutureStub.getTxByTxId(blockByTxIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            transactionInfo.setTransaction(transactionResp.getTransaction());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Archivecenter.TxDetailByIdRequest txDetailByIdRequest = Archivecenter.TxDetailByIdRequest.newBuilder().setTxId(txId).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        try {
            Archivecenter.TxDetailByIdResp txDetailByIdResp = archiveCenterServerFutureStub.getTxDetailByTxId(txDetailByIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            transactionInfo.setBlockHeight(txDetailByIdResp.getHeight()).setBlockTimestamp(txDetailByIdResp.getTxConfirmedTime());
        } catch (Exception e) {
            throw new ChainClientException("get getTxDetailByTxId err");
        }

        return transactionInfo.build();
    }

    @Override
    public ChainmakerTransaction.TransactionInfoWithRWSet getTxWithRWSetByTxId(String txId, long rpcCallTimeout) throws ChainClientException {
        Archivecenter.BlockByTxIdRequest blockByTxIdRequest = Archivecenter.BlockByTxIdRequest.newBuilder().setTxId(txId).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        ChainmakerTransaction.TransactionInfoWithRWSet.Builder builder = ChainmakerTransaction.TransactionInfoWithRWSet.newBuilder();
        try {
            Archivecenter.TransactionResp transactionResp = archiveCenterServerFutureStub.getTxByTxId(blockByTxIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            builder.setTransaction(transactionResp.getTransaction());
        } catch (Exception e) {
            throw new ChainClientException("");
        }
        Archivecenter.TxDetailByIdRequest txDetailByIdRequest = Archivecenter.TxDetailByIdRequest.newBuilder().setTxId(txId).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        try {
            Archivecenter.TxDetailByIdResp txDetailByIdResp = archiveCenterServerFutureStub.getTxDetailByTxId(txDetailByIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            builder.setBlockHeight(txDetailByIdResp.getHeight()).setBlockTimestamp(txDetailByIdResp.getTxConfirmedTime());
        } catch (Exception e) {
            throw new ChainClientException("");
        }
        try {
            Archivecenter.TxRWSetResp txRWSetResp = archiveCenterServerFutureStub.getTxRWSetByTxId(blockByTxIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            builder.setRwSet(txRWSetResp.getRwSet());
        } catch (Exception e) {
            throw new ChainClientException("");
        }

        return builder.build();
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        Archivecenter.BlockByHeightRequest blockByHeightRequest = Archivecenter.BlockByHeightRequest.newBuilder().setHeight(blockHeight).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        ChainmakerBlock.BlockInfo.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        try {
            Archivecenter.BlockWithRWSetResp blockWithRWSetResp = archiveCenterServerFutureStub.getBlockByHeight(blockByHeightRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            if (!blockWithRWSetResp.hasBlockData() || !blockWithRWSetResp.getBlockData().hasBlock()) {
                return null;
            }
            builder.setBlock(blockWithRWSetResp.getBlockData().getBlock());
            if (withRWSet) {
                builder.addAllRwsetList(blockWithRWSetResp.getBlockData().getRwsetListList());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ChainClientException(e.getMessage());
        }
        return builder.build();
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long rpcCallTimeout) throws ChainClientException{
        Archivecenter.BlockByHashRequest blockByHeightRequest = Archivecenter.BlockByHashRequest.newBuilder()
                .setBlockHash(blockHash)
                .setChainUnique(this.archiveCenterConfig.getChainGenesisHash())
                .setOperation(Archivecenter.OperationByHash.OperationGetBlockByHash)
                .build();
        ChainmakerBlock.BlockInfo.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        try {
            Archivecenter.BlockWithRWSetResp blockWithRWSetResp = archiveCenterServerFutureStub.getBlockByHash(blockByHeightRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            if (!blockWithRWSetResp.hasBlockData() || !blockWithRWSetResp.getBlockData().hasBlock()) {
                return null;
            }
            builder.setBlock(blockWithRWSetResp.getBlockData().getBlock());
            if (withRWSet) {
                builder.addAllRwsetList(blockWithRWSetResp.getBlockData().getRwsetListList());
            }
        } catch (Exception e) {
            throw new ChainClientException("");
        }
        return builder.build();
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        Archivecenter.BlockByTxIdRequest blockByTxIdRequest = Archivecenter.BlockByTxIdRequest.newBuilder().setTxId(txId).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        ChainmakerBlock.BlockInfo.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        try {
            Archivecenter.BlockWithRWSetResp blockWithRWSetResp = archiveCenterServerFutureStub.getBlockByTxId(blockByTxIdRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            if (!blockWithRWSetResp.hasBlockData() || !blockWithRWSetResp.getBlockData().hasBlock()) {
                return null;
            }
            builder.setBlock(blockWithRWSetResp.getBlockData().getBlock());
            if (withRWSet) {
                builder.addAllRwsetList(blockWithRWSetResp.getBlockData().getRwsetListList());
            }
        } catch (Exception e) {
            throw new ChainClientException("");
        }
        return builder.build();
    }

    @Override
    public ChainConfigOuterClass.ChainConfig getChainConfigByBlockHeight(long blockHeight, long rpcCallTimeout) throws ChainClientException {
        ChainmakerBlock.BlockInfo blockInfo = this.getBlockByHeight(blockHeight, false, rpcCallTimeout);
        if (blockInfo.getBlock().getHeader().getBlockType() == ChainmakerBlock.BlockType.CONFIG_BLOCK) {
            return getChainConfig(blockInfo.getBlock().getTxs(0));
        }
        long height = blockInfo.getBlock().getHeader().getPreConfHeight();
        blockInfo = this.getBlockByHeight(height, false, rpcCallTimeout);
        return getChainConfig(blockInfo.getBlock().getTxs(0));
    }

    private ChainConfigOuterClass.ChainConfig getChainConfig(ChainmakerTransaction.Transaction transaction)  throws ChainClientException{
        try {
            return ChainConfigOuterClass.ChainConfig.parseFrom(transaction.getResult().getContractResult().getResult().toByteArray());
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
    }

    @Override
    public void register(ChainmakerBlock.BlockInfo genesis, long rpcCallTimeout)  throws ChainClientException {
        Archivecenter.ArchiveStatusRequest archiveStatusRequest = Archivecenter.ArchiveStatusRequest.newBuilder().setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        Archivecenter.ArchiveStatusResp archiveStatusResp;
        try {
            archiveStatusResp = archiveCenterServerFutureStub.getArchivedStatus(archiveStatusRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            throw new ChainClientException("");
        }
        if (archiveStatusResp.getCode() == 0) {
            return;
        }
        // 获取区块hash
        String genesisHash = Hex.toHexString(genesis.getBlock().getHeader().getBlockHash().toByteArray());
        try {
            Archivecenter.ArchiveBlockRequest archiveBlockRequest = Archivecenter.ArchiveBlockRequest.newBuilder().setChainUnique(genesisHash).setBlock(genesis).build();
            Archivecenter.RegisterResp registerResp = archiveCenterServerFutureStub.register(archiveBlockRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
            if (registerResp.getCode() == 0 && registerResp.getRegisterStatus() == Archivecenter.RegisterStatus.RegisterStatusSuccess) {
                return;
            }
            throw new ChainClientException("register fail");
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
    }

    @Override
    public void archiveBlock(ChainmakerBlock.BlockInfo block) throws ChainClientException {
        try {
            StreamObserver<Archivecenter.SingleArchiveBlockResp> respStreamObserver = new StreamObserver<Archivecenter.SingleArchiveBlockResp>() {
                @Override
                public void onNext(Archivecenter.SingleArchiveBlockResp value) {
                    if (value == null) {
                        logger.error("archive resp code ");
                    }
                    logger.info("archive resp code {} ,message {} , begin {} , end {}", value.getCode(), value.getMessage(), value.getArchivedBeginHeight(), value.getArchivedEndHeight());
                }

                @Override
                public void onError(Throwable t) {
                    logger.error("archiveBlock err: {}", t.getMessage());
                }

                @Override
                public void onCompleted() {
                    logger.info("commit a block");
                }
            };
            StreamObserver<Archivecenter.ArchiveBlockRequest> requestStreamObserver = archiveCenterServerStub.singleArchiveBlocks(respStreamObserver);
            Archivecenter.ArchiveBlockRequest request = Archivecenter.ArchiveBlockRequest.newBuilder().setBlock(block).setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
            requestStreamObserver.onNext(request);
        } catch (Exception e) {
            throw new ChainClientException("", ExceptionType.ARCHIVEBLOCK);
        }
    }


    @Override
    public void archiveBlocks(BlockIterator blockIterator, Notice notice) throws ChainClientException{
        StreamObserver<Archivecenter.ArchiveBlockResp> respStreamObserver = new StreamObserver<Archivecenter.ArchiveBlockResp>() {
            @Override
            public void onNext(Archivecenter.ArchiveBlockResp value) {
                if (value == null) {
                    throw new RuntimeException();
                }
                if (value.getArchiveStatus() == Archivecenter.ArchiveStatus.ArchiveStatusFailed) {
                    logger.error("send failed {} ", value.getMessage());
                }
                logger.info("archive resp code {} ,message {}", value.getCode(), value.getMessage());
            }

            @Override
            public void onError(Throwable t) {
                notice.heightNotice(new ProcessMessage(t.getMessage()));
            }

            @Override
            public void onCompleted() {
                logger.info("commit a block");
            }
        };
        StreamObserver<Archivecenter.ArchiveBlockRequest> requestStreamObserver = archiveCenterServerStub.archiveBlocks(respStreamObserver);
        if (blockIterator == null) {
            return;
        }

        int sendCount = 0;
        // 迭代
        while (blockIterator.next()) {
            ChainmakerBlock.BlockInfo blockInfo = null;
            try {
                blockInfo = blockIterator.value();
            } catch (ChainClientException e) {
                notice.heightNotice(new ProcessMessage(blockIterator.current(), e.getMessage()));
                continue;
            }
            Archivecenter.ArchiveBlockRequest request = Archivecenter.ArchiveBlockRequest.newBuilder()
                    .setChainUnique(this.archiveCenterConfig.getChainGenesisHash())
                    .setBlock(blockInfo)
                    .build();
            requestStreamObserver.onNext(request);
            sendCount ++;
            notice.heightNotice(new ProcessMessage(blockInfo.getBlock().getHeader().getBlockHeight()));
        }
        if (sendCount == 0) {
            throw new ChainClientException("no block to archive");
        }
        blockIterator.release();
    }


    @Override
    public Archivecenter.ArchiveStatusResp getArchivedStatus(long rpcCallTimeout) throws ChainClientException {
        Archivecenter.ArchiveStatusRequest archiveStatusRequest = Archivecenter.ArchiveStatusRequest.newBuilder().setChainUnique(this.archiveCenterConfig.getChainGenesisHash()).build();
        Archivecenter.ArchiveStatusResp archiveStatusResp;
        try {
            archiveStatusResp = archiveCenterServerFutureStub.getArchivedStatus(archiveStatusRequest).get(rpcCallTimeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            throw new ChainClientException(e.getMessage());
        }
        return archiveStatusResp;
    }

    private void addNettyBuilderProps(NettyChannelBuilder channelBuilder, Properties props) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (props == null) {
            return;
        }

        for (Map.Entry<?, ?> es : props.entrySet()) {
            Object methodprop = es.getKey();
            if (methodprop == null) {
                continue;
            }
            String methodName = String.valueOf(methodprop);
            Object parmsArrayO = es.getValue();
            Object[] parmsArray;
            if (!(parmsArrayO instanceof Object[])) {
                parmsArray = new Object[]{parmsArrayO};
            } else {
                parmsArray = (Object[]) parmsArrayO;
            }

            Class<?>[] classParams = getClassParams(parmsArray);
            final Method method = channelBuilder.getClass().getMethod(methodName, classParams);

            method.invoke(channelBuilder, parmsArray);
        }
    }

    private Class<?>[] getClassParams(Object[] parmsArray) {
        Class<?>[] classParams = new Class[parmsArray.length];
        int i = -1;
        for (Object oparm : parmsArray) {
            ++i;
            if (null == oparm) {
                classParams[i] = Object.class;
                continue;
            }

            Class<?> unwrapped = WRAPPERS_TO_PRIM.get(oparm.getClass());
            if (null != unwrapped) {
                classParams[i] = unwrapped;
                continue;
            }

            Class<?> clz = oparm.getClass();

            Class<?> ecz = clz.getEnclosingClass();
            if (null != ecz && ecz.isEnum()) {
                clz = ecz;
            }
            classParams[i] = clz;
        }
        return classParams;
    }

}
