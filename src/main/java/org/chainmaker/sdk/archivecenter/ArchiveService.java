/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import org.chainmaker.pb.archivecenter.Archivecenter;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.sdk.ChainClientException;

/**
 * 归档方法接口
 */
public interface ArchiveService {

    /**
     * 根据交易id获取交易
     * @param txId 交易id
     * @param rpcCallTimeout 超时时间
     * @return 交易
     */
    ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long rpcCallTimeout) throws ChainClientException;

    /**
     * 根据交易id获取包含rwset的交易
     * @param txId 交易id
     * @param rpcCallTimeout 超时时间
     * @return 包含rwset的交易
     */
    ChainmakerTransaction.TransactionInfoWithRWSet getTxWithRWSetByTxId(String txId, long rpcCallTimeout) throws ChainClientException;

    /**
     * 根据区块高度获取区块
     * @param blockHeight 区块高度
     * @param withRWSet 是否包含读写集
     * @param rpcCallTimeout 超时时间
     * @return 区块信息
     */
    ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long rpcCallTimeout) throws ChainClientException;

    /**
     * 根据区块哈希获取区块
     * @param blockHash 区块哈希
     * @param withRWSet 是否包含读写集
     * @param rpcCallTimeout 超时时间
     * @return 区块信息
     */
    ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long rpcCallTimeout) throws ChainClientException;

    /**
     * 根据交易id获取区块
     * @param txId 交易id
     * @param withRWSet 是否包含读写集
     * @param rpcCallTimeout 超时时间
     * @return 区块信息
     */
    ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long rpcCallTimeout) throws ChainClientException;

    /**
     * 根据区块高度获取链配置
     * @param blockHeight 区块高度
     * @param rpcCallTimeout 超时时间
     * @return 区块信息
     */
    ChainConfigOuterClass.ChainConfig getChainConfigByBlockHeight(long blockHeight, long rpcCallTimeout) throws ChainClientException;

    /**
     * 注册区块
     * @param genesis 链的第一个区块
     * @param rpcCallTimeout 超时时间
     */
    void register(ChainmakerBlock.BlockInfo genesis, long rpcCallTimeout) throws ChainClientException;

    /**
     * 归档单个区块
     * @param block 区块
     */
    void archiveBlock(ChainmakerBlock.BlockInfo block) throws ChainClientException;

    /**
     * 批量归档区块
     * @param blockIterator 区块迭代器
     */
    void archiveBlocks(BlockIterator blockIterator, Notice notice) throws ChainClientException;

    /**
     * 获取归档状态
     * @param rpcCallTimeout 超时时间
     */
    Archivecenter.ArchiveStatusResp getArchivedStatus(long rpcCallTimeout) throws ChainClientException;

}
