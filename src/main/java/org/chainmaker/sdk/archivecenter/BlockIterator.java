/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.sdk.ChainClientException;

public interface BlockIterator {

    // 判断是否继续执行
    Boolean next();

    // 获取当前区块
    ChainmakerBlock.BlockInfo value() throws ChainClientException;

    // 发版
    void release();

    // 获取区块总数
    long total();

    // 获取当前高度
    long current();

}
