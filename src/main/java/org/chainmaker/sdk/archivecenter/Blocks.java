/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.sdk.ChainClient;
import org.chainmaker.sdk.ChainClientException;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;

/**
 * 区块迭代器的实现对象
 */
public class Blocks implements BlockIterator {

    // 当前执行的区块高度
    private long height;

    // 结束的区块高度
    private long endHeight;

    // 执行总数
    private long total;

    // 通知对象
    private Notice notice;

    // client 用于执行获取区块
    private ChainClient chainClient;

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getEndHeight() {
        return endHeight;
    }

    public void setEndHeight(long endHeight) {
        this.endHeight = endHeight;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

    public ChainClient getChainClient() {
        return chainClient;
    }

    public void setChainClient(ChainClient chainClient) {
        this.chainClient = chainClient;
    }

    public Blocks(long height, long endHeight, long total, Notice notice, ChainClient chainClient) {
        this.height = height;
        this.endHeight = endHeight;
        this.total = total;
        this.notice = notice;
        this.chainClient = chainClient;
    }

    @Override
    public Boolean next() {
        return height++ < endHeight;
    }

    @Override
    public ChainmakerBlock.BlockInfo value() throws ChainClientException {
        try {
            return chainClient.getBlockByHeight(height, true, 10000);
        } catch (ChainMakerCryptoSuiteException e) {
            throw new ChainClientException(e.getMessage());
        }
    }

    @Override
    public void release() {

    }

    @Override
    public long total() {
        return total;
    }

    @Override
    public long current() {
        return height;
    }
}
