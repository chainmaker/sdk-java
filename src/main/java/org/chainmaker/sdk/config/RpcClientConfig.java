/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.config;

public class RpcClientConfig {
    private int maxReceiveMessageSize = 16;

    private String proxyAddr;

    public int getMax_receive_message_size() {
        return maxReceiveMessageSize;
    }

    public void setMax_receive_message_size(int max_receive_message_size) {
        this.maxReceiveMessageSize = max_receive_message_size;
    }

    public int getMaxReceiveMessageSize() {
        return maxReceiveMessageSize;
    }

    public void setMaxReceiveMessageSize(int maxReceiveMessageSize) {
        this.maxReceiveMessageSize = maxReceiveMessageSize;
    }

    public String getProxy_addr() {
        return proxyAddr;
    }

    public void setProxy_addr(String proxy_addr) {
        this.proxyAddr = proxy_addr;
    }

    public String getProxyAddr() {
        return proxyAddr;
    }

    public void setProxyAddr(String proxyAddr) {
        this.proxyAddr = proxyAddr;
    }
}
