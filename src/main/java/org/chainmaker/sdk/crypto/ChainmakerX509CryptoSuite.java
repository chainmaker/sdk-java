/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.crypto;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

// ChainmakerX509CryptoSuite is a security library suite used to sign payload, store ca trust roots, etc.
public class ChainmakerX509CryptoSuite implements CryptoSuite {
    private static final String HASH_ALGORITHM = "SHA256";
    private static final String CERTIFICATE_FORMAT = "X.509";
    private static final String ALGORITHM_SM2_KEY = "SM3withSM2";
    private static final String ALGORITHM_RSA = "SHA256withRSA";
    private static final String ALGORITHM_ECDSA = "SHA256withECDSA";

    private static final String OS_NAME_MAC = "mac";
    private static final String OS_NAME_WIN = "win";
    private static final String OS_NAME_LINUX = "linux";
    private static final String OS_ARCH_AMD64 = "amd64";
    private static final String OS_ARCH_X86_64= "x86_64";
    private static final String OS_ARCH_ARM64= "aarch64";


    private String algorithm;

    private static AtomicBoolean initialized = new AtomicBoolean(true);

    private static final Logger logger = LoggerFactory.getLogger(ChainmakerX509CryptoSuite.class);

    public static CustomSignInterface cryptoHsmInterface;
    public static void setCryptoHsmInterface(CustomSignInterface chi) {
        cryptoHsmInterface = chi;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    static {
//        try {
//            enableX509CertificateWithGM();
//        } catch (Exception e) {
//            logger.error("CurveDB enableGM err : ", e);
//        }
//        System.out.println(Arrays.toString(Security.getProviders()));
        try {
            loadLibInfo();
            loadAlgorithms();
        } catch (Exception e) {
            logger.error("CurveDB enableGM err : ", e);
        }

    }

    private static final List<String> HASH_TYPE_SET = Arrays.asList(HASH_ALGORITHM, "SM3", "SHA3");

    private static final String curveName = "secp384r1";

    private KeyStore trustStore = null;

    // new crypto suite instancezx
    public static ChainmakerX509CryptoSuite newInstance() throws ChainMakerCryptoSuiteException {
        return new ChainmakerX509CryptoSuite();
    }

    public static ChainmakerX509CryptoSuite newInstance(Boolean pkcs11Enable) throws ChainMakerCryptoSuiteException {
        if (pkcs11Enable && initialized.compareAndSet(true, false)) {
            if (cryptoHsmInterface != null) {
                cryptoHsmInterface.init();
            } else {
                swxaInit();
            }
        }
        return new ChainmakerX509CryptoSuite();
    }

    private ChainmakerX509CryptoSuite() throws ChainMakerCryptoSuiteException {
        createTrustStore();
    }

    // load ca certificates
    @Override
    public void loadCACertificates(Collection<Certificate> certificates) throws ChainMakerCryptoSuiteException {
        if (CollectionUtils.isEmpty(certificates)) {
            throw new ChainMakerCryptoSuiteException("Unable to load CA certificates. List is empty");
        }

        try {
            for (Certificate cert : certificates) {
                addCACertificateToTrustStore(cert);
            }
        } catch (Exception e) {
            // Note: This can currently never happen (as cert<>null and alias<>null)
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    // load ca certificates from certificate bytes
    @Override
    public void loadCACertificatesAsBytes(Collection<byte[]> certificates) throws ChainMakerCryptoSuiteException {
        if (CollectionUtils.isEmpty(certificates)) {
            throw new ChainMakerCryptoSuiteException("List of CA certificates is empty. Nothing to load.");
        }

        ArrayList<Certificate> certList = new ArrayList<>();
        for (byte[] certBytes : certificates) {
            certList.add(getCertificateFromBytes(certBytes));
        }
        loadCACertificates(certList);
    }

    private void addCACertificateToTrustStore(Certificate certificate) throws ChainMakerCryptoSuiteException {
        String alias;
        if (certificate instanceof X509Certificate) {
            alias = ((X509Certificate) certificate).getSerialNumber().toString();
        } else { // not likely ...
            alias = Integer.toString(certificate.hashCode());
        }
        addCACertificateToTrustStore(certificate, alias);
    }

    private void addCACertificateToTrustStore(Certificate caCert, String alias) throws ChainMakerCryptoSuiteException {

        if (alias == null || alias.isEmpty()) {
            throw new ChainMakerCryptoSuiteException("You must assign an alias to a certificate when adding to the trust store.");
        }

        if (caCert == null) {
            throw new ChainMakerCryptoSuiteException("Certificate cannot be null.");
        }

        try {
            if (trustStore.containsAlias(alias)) {
                return;
            }

            trustStore.setCertificateEntry(alias, caCert);

        } catch (KeyStoreException e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    private void createTrustStore() throws ChainMakerCryptoSuiteException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            this.trustStore = keyStore;
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    // generate key pair
    @Override
    public KeyPair keyGen() throws ChainMakerCryptoSuiteException {
        return ecdsaKeyGen();
    }

    private KeyPair ecdsaKeyGen() throws ChainMakerCryptoSuiteException {
        return generateKey("EC", curveName);
    }

    private KeyPair generateKey(String encryptionName, String curveName) throws ChainMakerCryptoSuiteException {
        try {
            ECGenParameterSpec ecGenSpec = new ECGenParameterSpec(curveName);
            KeyPairGenerator g = KeyPairGenerator.getInstance(encryptionName);
            g.initialize(ecGenSpec, new SecureRandom());
            return g.generateKeyPair();
        } catch (Exception exp) {
            throw new ChainMakerCryptoSuiteException(exp.toString());
        }
    }

    // sign payload using private key
    @Override
    public byte[] sign(PrivateKey privateKey, byte[] plainText) throws ChainMakerCryptoSuiteException {
        if (plainText == null || plainText.length == 0) {
            throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
        }
        try {
            Signature sig = Signature.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME);
            sig.initSign(privateKey);
            sig.update(plainText);
            return sig.sign();

        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    // sign with swxa
    @Override
    public byte[] signWithHsm(Integer keyId, String algo, byte[] plainText) throws ChainMakerCryptoSuiteException {
        if (plainText == null || plainText.length == 0) {
            throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
        }
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(algo, "SwxaJCE");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        kpg.initialize((keyId * 2 - 1) << 16);
        KeyPair keyPair = kpg.genKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        Signature signature = null;
        try {
            signature = Signature.getInstance(algorithm, "SwxaJCE");
            signature.initSign(privateKey);
            signature.update(plainText);
            return signature.sign();
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    // test
    public boolean verifyWithHsm(Certificate certificate, byte[] sign, byte[] plainText) throws ChainMakerCryptoSuiteException {
        PublicKey publicKey = certificate.getPublicKey();
        Signature signature = null;
        try {
            signature = Signature.getInstance(algorithm, "SwxaJCE");
            signature.initVerify(publicKey);
            signature.update(plainText);
            return signature.verify(sign);
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }



    @Override
    public byte[] rsaSign(PrivateKey privateKey, byte[] plainText, String hash) throws ChainMakerCryptoSuiteException {
        if (plainText == null || plainText.length == 0) {
            throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
        }
        //NONEwithRSA
        try {
            Signature sig;
            if (hash != null && hash.equals("SM3")) {
                sig = Signature.getInstance(ALGORITHM_SM2_KEY, BouncyCastleProvider.PROVIDER_NAME);
            }else if (Objects.equals(privateKey.getAlgorithm(), "ECDSA")) {
                sig = Signature.getInstance(ALGORITHM_ECDSA, BouncyCastleProvider.PROVIDER_NAME);
            } else {
                sig = Signature.getInstance(ALGORITHM_RSA, BouncyCastleProvider.PROVIDER_NAME);
            }
            sig.initSign(privateKey);
            sig.update(plainText);
            return sig.sign();
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }

    // verify the signature according certificate
    @Override
    public boolean verify(Certificate certificate, byte[] signature, byte[] plainText) throws ChainMakerCryptoSuiteException {
        boolean isVerified = false;

        try {
            Signature sig = Signature.getInstance(algorithm);
            sig.initVerify(certificate);
            sig.update(plainText);
            isVerified = sig.verify(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            logger.error("verify fail : ", e);
            throw new ChainMakerCryptoSuiteException("verify fail fail : " + e.getMessage());
        }

        return isVerified;
    }

    // hash bytes and return the hash value
    @Override
    public byte[] hash(byte[] plainText) throws ChainMakerCryptoSuiteException {
        Digest digest = getHashDigest();
        byte[] retValue = new byte[digest.getDigestSize()];
        digest.update(plainText, 0, plainText.length);
        digest.doFinal(retValue, 0);
        return retValue;
    }

    private Digest getHashDigest() throws ChainMakerCryptoSuiteException {

        if (!HASH_TYPE_SET.contains(HASH_ALGORITHM.toUpperCase())) {
            throw new ChainMakerCryptoSuiteException("hash algorithm not support");
        }
        return new SHA256Digest();
    }

    // Get certificate from bytes
    @Override
    public Certificate getCertificateFromBytes(byte[] certBytes) throws ChainMakerCryptoSuiteException {
        if (certBytes == null || certBytes.length == 0) {
            throw new ChainMakerCryptoSuiteException("bytesToCertificate: input null or zero length");
        }
        return getX509Certificate(certBytes);
    }

    private X509Certificate getX509Certificate(byte[] pemCertificate) throws ChainMakerCryptoSuiteException {
        X509Certificate ret = null;

        try {
            CertificateFactory cf = CertificateFactory.getInstance(CERTIFICATE_FORMAT, BouncyCastleProvider.PROVIDER_NAME);
            ByteArrayInputStream certInputStream = new ByteArrayInputStream(pemCertificate);
            ret = (X509Certificate) cf.generateCertificate(certInputStream);
        } catch (CertificateException | NoSuchProviderException e) {
            logger.error("convert pem bytes fail : ", e);
            throw new ChainMakerCryptoSuiteException("convert pem bytes fail : " + e.getMessage());
        }
        if (ret == null) {
            throw new ChainMakerCryptoSuiteException("can't convert pem bytes");
        }
        this.algorithm = ret.getSigAlgName();
        return ret;
    }

    private static ECPublicKeyParameters convertPublicKeyToParameters(BCECPublicKey ecPubKey) {
        ECParameterSpec parameterSpec = ecPubKey.getParameters();
        ECDomainParameters domainParameters = new ECDomainParameters(parameterSpec.getCurve(), parameterSpec.getG(),
                parameterSpec.getN(), parameterSpec.getH());
        return new ECPublicKeyParameters(ecPubKey.getQ(), domainParameters);
    }

    private static void enableX509CertificateWithGM() throws IllegalAccessException, InvocationTargetException,
            NoSuchFieldException, ClassNotFoundException, IOException {
        Class curveDBClazz = getaCurveDBClass();

        if (curveDBClazz == null) {
            throw new NoSuchFieldException();
        }
        Method[] methods = curveDBClazz.getDeclaredMethods();
        Method method = null;

        Pattern splitPattern = Pattern.compile(",|\\[|\\]");
        for (Method m : methods) {
            if ("add".equals(m.getName())) {
                method = m;
            }
        }
        if (method == null) {
            throw new NoSuchFieldException();
        }
        method.setAccessible(true);
        method.invoke(curveDBClazz, "sm2p256v1", "1.2.156.10197.1.301", 1,
                "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF",
                "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC",
                "28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93",
                "32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7",
                "BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0",
                "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123",
                1, splitPattern);

        final Field specCollection = curveDBClazz.getDeclaredField("specCollection");
        final Field oidMap = curveDBClazz.getDeclaredField("oidMap");
        oidMap.setAccessible(true);
        specCollection.setAccessible(true);
        specCollection.set(curveDBClazz, Collections.unmodifiableCollection(((Map) oidMap.get(curveDBClazz)).values()));

        //sun.security.util.AlgorithmId
        Class algorithmIdClazz = getaAlgorithmIdClass();

        if (algorithmIdClazz == null) {
            throw new NoSuchFieldException();
        }
        Field nameTable = algorithmIdClazz.getDeclaredField("nameTable");
        nameTable.setAccessible(true);

        Class ObjectIdentifierClazz = getaObjectIdentifierClass();

        if (ObjectIdentifierClazz == null) {
            throw new NoSuchFieldException();
        }
        methods = ObjectIdentifierClazz.getDeclaredMethods();


        for (Method m : methods) {
            if ("newInternal".equals(m.getName())) {
                method = m;
            }
        }
        if (method == null) {
            throw new NoSuchFieldException();
        }
        method.setAccessible(true);

        Object o = method.invoke(ObjectIdentifierClazz, new int[]{1, 2, 156, 10197, 1, 501});

        ((HashMap) nameTable.get(algorithmIdClazz)).put(o, ALGORITHM_SM2_KEY);
        // Map<ObjectIdentifier, String> map = (HashMap) nameTable.get(algorithmIdClazz);
        // ObjectIdentifier objectIdentifier = ObjectIdentifier.newInternal(new int[]{1, 2, 156, 10197, 1, 501});

        // map.put(objectIdentifier, ALGORITHM_SM2_KEY);

        Class clazz = Class.forName("io.netty.handler.ssl.ExtendedOpenSslSession");
        Field algorithmsField = clazz.getDeclaredField("LOCAL_SUPPORTED_SIGNATURE_ALGORITHMS");
        algorithmsField.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(algorithmsField, algorithmsField.getModifiers() & ~Modifier.FINAL);
        String[] algorithms = (String[]) algorithmsField.get(null);
        String[] newAlgorithms = new String[algorithms.length + 1];
        System.arraycopy(algorithms, 0, newAlgorithms, 0, algorithms.length);
        newAlgorithms[algorithms.length] = ALGORITHM_SM2_KEY;
        algorithmsField.set(null, newAlgorithms);

        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {
            loadLib("libcrypto-1_1-x64");
            loadLib("libssl-1_1-x64");
        }
    }

    private static void loadGmProtocol() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Class.forName("io.netty.handler.ssl.OpenSsl");
        Field field = clazz.getDeclaredField("SUPPORTED_PROTOCOLS_SET");
        Method getDeclaredFields0 = Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class);
        getDeclaredFields0.setAccessible(true);
        Field[] fields = (Field[]) getDeclaredFields0.invoke(Field.class, false);
        Field modifiers = null;
        for (Field each : fields) {
            if ("modifiers".equals(each.getName())) {
                modifiers = each;
            }
        }
        modifiers.setAccessible(true);
        // 去除final
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        //将字段的访问权限设为true：即去除private修饰符的影响
        field.setAccessible(true);
        Set<String> Algorithms = (Set<String>) field.get(null);
        Set<String> newSets = new HashSet<>(Algorithms);
        newSets.add("GMTLSv1.1");
        field.set(null, newSets);
        // 恢复final
        modifiers.setInt(field, field.getModifiers() | Modifier.FINAL);
    }


    private static void loadGmCipherSuites() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Class.forName("io.netty.handler.ssl.OpenSsl");
        Field field = clazz.getDeclaredField("AVAILABLE_OPENSSL_CIPHER_SUITES");
        Method getDeclaredFields0 = Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class);
        getDeclaredFields0.setAccessible(true);
        Field[] fields = (Field[]) getDeclaredFields0.invoke(Field.class, false);
        Field modifiers = null;
        for (Field each : fields) {
            if ("modifiers".equals(each.getName())) {
                modifiers = each;
            }
        }
        modifiers.setAccessible(true);
        // 去除final
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        //将字段的访问权限设为true：即去除private修饰符的影响
        field.setAccessible(true);
        Set<String> Algorithms = (Set<String>) field.get(null);
        Set<String> newList = new HashSet<>(Algorithms);
        newList.add("ECDHE-SM4-GCM-SM3");
        newList.add("ECC-SM4-SM3");
        newList.add("ECDHE-SM4-SM3");
        newList.add("ECC-SM4-GCM-SM3");
        newList.add("TLS_SM4_GCM_SM3");
        newList.add("TLS_SM4_CCM_SM3");
        newList.add("UNKNOWN_RSA_WITH_ECDHE_SM4_GCM_SM3");
        newList.add("UNKNOWN_RSA_WITH_ECC_SM4_SM3");
        newList.add("UNKNOWN_RSA_WITH_ECC_SM4_GCM_SM3");
        newList.add("UNKNOWN_RSA_WITH_ECDHE_SM4_SM3");

        field.set(null, newList);
        // 恢复final
        modifiers.setInt(field, field.getModifiers() | Modifier.FINAL);
    }

    private static void loadAlgorithms() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Class.forName("io.netty.handler.ssl.ExtendedOpenSslSession");
        Field field = clazz.getDeclaredField("LOCAL_SUPPORTED_SIGNATURE_ALGORITHMS");
        Method getDeclaredFields0 = Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class);
        getDeclaredFields0.setAccessible(true);
        Field[] fields = (Field[]) getDeclaredFields0.invoke(Field.class, false);
        Field modifiers = null;
        for (Field each : fields) {
            if ("modifiers".equals(each.getName())) {
                modifiers = each;
            }
        }
        modifiers.setAccessible(true);
        // 去除final
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        //将字段的访问权限设为true：即去除private修饰符的影响
        field.setAccessible(true);
        String[] Algorithms = (String[]) field.get(null);
        List<String> list  = new ArrayList<>(Arrays.asList(Algorithms));
        list.add("1.2.156.10197.1.501");
        list.add("SM3withSM2");
        String[] newAlgorithms = new String[list.size()];
        list.toArray(newAlgorithms);
        field.set(null, newAlgorithms);
        // 恢复final
        modifiers.setInt(field, field.getModifiers() | Modifier.FINAL);
        // load 国密协议
        loadGmProtocol();
        // load 国密套件
        loadGmCipherSuites();
    }

    private static void loadLibInfo() throws Exception {
//        BouncyCastleProvider bouncyCastleProvider = (BouncyCastleProvider) Security.getProvider( BouncyCastleProvider.PROVIDER_NAME);
//        bouncyCastleProvider.put("KeyManagerFactory.SUNX509", "sun.security.ssl.KeyManagerFactoryImpl$SunX509");
//        bouncyCastleProvider.put("TrustManagerFactory.PKIX", "sun.security.ssl.TrustManagerFactoryImpl$PKIXFactory");
//        bouncyCastleProvider.put("TrustManagerFactory.PKIX", "org.chainmaker.sdk.crypto.TrustManagerFactory$BCTrustManagerFactory");
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
        Security.insertProviderAt(new BouncyCastleProvider(), 3);
        String architecture = System.getProperty("os.arch");
        String osname = System.getProperty("os.name").toLowerCase();
        String sslName = "libssl";
        String cryptoName = "libcrypto";
        boolean isAmd64 = architecture.toLowerCase().startsWith(OS_ARCH_X86_64) || architecture.toLowerCase().startsWith(OS_ARCH_AMD64);
        if (!osname.startsWith(OS_NAME_WIN)) {
            if (isAmd64) {
                sslName = getLibsslName(OS_ARCH_X86_64);
                cryptoName = getLibcryptoName(OS_ARCH_X86_64);
            }
            if (architecture.toLowerCase().startsWith(OS_ARCH_ARM64)) {
                sslName = getLibsslName(OS_ARCH_ARM64);
                cryptoName = getLibcryptoName(OS_ARCH_ARM64);
            }
        }
        loadLib(cryptoName + getNativeExt(osname), "libcrypto", getExt(osname));
        loadLib(sslName + getNativeExt(osname), "libssl", getExt(osname));
    }

    private static  String getLibsslName(String arch) {
        return "libssl-" + arch;
    }

    private static String getLibcryptoName(String arch) {
        return "libcrypto-" + arch;
    }

    private static  String getExt(String os) {
        if (os.startsWith(OS_NAME_WIN)) {
            return "-1_1-x64.dll";
        } else if (os.startsWith(OS_NAME_MAC)) {
            return ".1.1.dylib";
        } else if (os.startsWith(OS_NAME_LINUX)) {
            return ".so.1.1";
        }
        return "";
    }

    private static String getNativeExt(String os) {
        if (os.startsWith(OS_NAME_WIN)) {
            return "-1_1-x64.dll";
        } else if (os.startsWith(OS_NAME_MAC)) {
            return ".dylib";
        } else if (os.startsWith(OS_NAME_LINUX)) {
            return ".so";
        }
        return "";
    }

    private static Class getaAlgorithmIdClass() throws ClassNotFoundException {
        Class algorithmIdClazz = null;
        try {
            algorithmIdClazz = Class.forName("sun.security.x509.AlgorithmId");
        } catch (ClassNotFoundException e) {
            try {
                algorithmIdClazz = Class.forName("java.security.x509.AlgorithmId");
            }catch (ClassNotFoundException e1) {
                logger.error("not found  AlgorithmId: ", e1);
            }

        }
        return algorithmIdClazz;
    }

    private static Class getaObjectIdentifierClass() throws ClassNotFoundException {
        Class ObjectIdentifierClazz = null;
        try {
            ObjectIdentifierClazz = Class.forName("sun.security.util.ObjectIdentifier");
        } catch (ClassNotFoundException e) {
            try {
                ObjectIdentifierClazz = Class.forName("java.security.util.ObjectIdentifier");
            }catch (ClassNotFoundException e1){
                logger.error("not found  ObjectIdentifier: ", e1);
            }

        }
        return ObjectIdentifierClazz;
    }

    private static Class getaObjectSwxaClass() throws ClassNotFoundException {
        Class ObjectSwxaClazz = null;
        try {
            ObjectSwxaClazz = Class.forName("com.sansec.jce.provider.SwxaProvider");
        } catch (ClassNotFoundException e) {
            logger.error("not found  ObjectSwxaClazz: ", e);
        }
        return ObjectSwxaClazz;
    }

    private static Class getaCurveDBClass() throws ClassNotFoundException {
        Class curveDBClazz = null;
        try {
            curveDBClazz = Class.forName("sun.security.util.CurveDB");
        } catch (ClassNotFoundException e) {
            try {
                curveDBClazz = Class.forName("sun.security.ec.CurveDB");
            } catch (ClassNotFoundException e1) {
                try {
                    curveDBClazz = Class.forName("java.security.ec.CurveDB");
                } catch (Exception e2) {
                    logger.error("not found  CurveDB: ", e2);
                }
            }
        }
        return curveDBClazz;
    }

    private static void loadLib(String libName) throws IOException {
        loadLib(libName, libName, ".dll");
    }

    private static void loadLib(String libName, String tmpLibName, String libExtension) throws IOException {

        String libFullName = libName;

        String nativeTempDir = System.getProperty("java.io.tmpdir");

        InputStream in = null;
        BufferedInputStream reader = null;
        FileOutputStream writer = null;
        File extractedLibFile = new File(nativeTempDir + File.separator + tmpLibName + libExtension);
        try {
            ClassLoader classLoader = ChainmakerX509CryptoSuite.class.getClassLoader();
            in = classLoader.getResourceAsStream("META-INF/native/" + libFullName);
            if (in == null) {
                in = ChainmakerX509CryptoSuite.class.getResourceAsStream("/win32-x86-64/" + libFullName);
            }
            if (in == null) {
                in = ChainmakerX509CryptoSuite.class.getResourceAsStream(libFullName);
            }
            if (in == null) {
                return;
            }
            ChainmakerX509CryptoSuite.class.getResource(libFullName);
            reader = new BufferedInputStream(in);
            writer = new FileOutputStream(extractedLibFile);

            byte[] buffer = new byte[1024];

            while (reader.read(buffer) > 0) {
                writer.write(buffer);
                buffer = new byte[1024];
            }
        } catch (IOException e) {
            logger.error("io exception is {} ", e.getMessage());
        } finally {
            if (in != null) {
                in.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
        System.load(extractedLibFile.toString());
        if (extractedLibFile.exists()) {
            extractedLibFile.deleteOnExit();
        }
    }

    public static void swxaInit() throws ChainMakerCryptoSuiteException {
        Object clazz;
        try {
            clazz = getaObjectSwxaClass().newInstance();
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException("Unable to load CA certificates. List is empty");
        }
        if (clazz instanceof Provider) {
            Security.addProvider((Provider) clazz);
        }
    }

    @Override
    public void init() throws ChainMakerCryptoSuiteException {

    }

    @Override
    public byte[] signWithCustom(byte[] plainText, Map params) throws ChainMakerCryptoSuiteException {
        if (cryptoHsmInterface != null) {
            return cryptoHsmInterface.signWithCustom(plainText, params);
        }
        if (plainText == null || plainText.length == 0) {
            throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
        }
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance((String) params.get("keyType"), "SwxaJCE");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        kpg.initialize(((Integer) params.get("keyId") * 2 - 1) << 16);
        KeyPair keyPair = kpg.genKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        Signature signature = null;
        try {
            signature = Signature.getInstance((String) params.get("algorithm"), "SwxaJCE");
            signature.initSign(privateKey);
            signature.update(plainText);
            return signature.sign();
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
    }


    public static String KEY_ID = "key_id";

    public static String KEY_TYPE = "key_type";

    @Override
    public Map parseUserKey(byte[] userKeyBytes) throws ChainMakerCryptoSuiteException {
        if (cryptoHsmInterface != null) {
            return cryptoHsmInterface.parseUserKey(userKeyBytes);
        }
        JSONObject jsonObject = JSONObject.parseObject(new String(userKeyBytes));
        Map map = new HashMap<>();
        map.put("keyId", (String) jsonObject.get(KEY_ID));
        map.put("keyType", (String) jsonObject.get(KEY_TYPE));
        return map;
    }
}
