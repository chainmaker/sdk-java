/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.crypto;


import java.util.Map;

public interface CustomSignInterface {

    /**
     * 自定义签名初始化
     * @throws ChainMakerCryptoSuiteException
     */
    void init() throws ChainMakerCryptoSuiteException;

    /**
     * 自定义签名
     * @param plainText 签名原文
     * @param params 签名需要的参数
     * @return 签名数据
     * @throws ChainMakerCryptoSuiteException
     */
    byte[] signWithCustom(byte[] plainText, Map params) throws ChainMakerCryptoSuiteException;

    /**
     * 根据用户传入的key值解析出signWithCustom所需要的参数
     * @param userKeyBytes 用户私钥
     * @return 解析结果
     * @throws ChainMakerCryptoSuiteException
     */
    Map parseUserKey(byte[] userKeyBytes) throws ChainMakerCryptoSuiteException;
}
