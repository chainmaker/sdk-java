/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.model;

/**
 * 区块信息
 */
public class BlockInfo extends BaseModel{

    // 链id
    private String fchainId;

    // 区块高度
    private long fblockHeight;

    // 带有读写集的区块信息
    private byte[] fblockWithRwset;

    // calculate hash
    private String fhmac;

    // 是否归档 默认0-归档
    private boolean fisArchived;

    public final static Integer rowsPerBlockInfoTable = 10000;
    public final static String prefixBlockInfoTable = "t_block_info";


    public String getFchainId() {
        return fchainId;
    }

    public void setFchainId(String fchainId) {
        this.fchainId = fchainId;
    }

    public long getFblockHeight() {
        return fblockHeight;
    }

    public void setFblockHeight(long fblockHeight) {
        this.fblockHeight = fblockHeight;
    }

    public byte[] getFblockWithRwset() {
        return fblockWithRwset;
    }

    public void setFblockWithRwset(byte[] fblockWithRwset) {
        this.fblockWithRwset = fblockWithRwset;
    }

    public String getFhmac() {
        return fhmac;
    }

    public void setFhmac(String fhmac) {
        this.fhmac = fhmac;
    }

    public boolean isFisArchived() {
        return fisArchived;
    }

    public void setFisArchived(boolean fisArchived) {
        this.fisArchived = fisArchived;
    }

    public static String BlockInfoTableNameByBlockHeight(long blkHeight) {
        long tableName = blkHeight / rowsPerBlockInfoTable + 1;
        return prefixBlockInfoTable + "_" + tableName;
    }
}
