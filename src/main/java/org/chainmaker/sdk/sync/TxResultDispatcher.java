/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.sync;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.ChainClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

public class TxResultDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(TxResultDispatcher.class);


    // block location of subscription start
    private long nextBlockNum;

    private Status status;

    // thread running subscription operation

    private ExecutorService executorService;


    // chain configuration
    private final ChainClient chainClient;

    //
    private final ConcurrentHashMap<String, Result> result;

    private long rpcCallTimeout = 5000;


    class Result {
        ChainmakerTransaction.Transaction tx;

        public ChainmakerTransaction.Transaction getTx() {
            return tx;
        }

        public void setTx(ChainmakerTransaction.Transaction tx) {
            this.tx = tx;
        }
    }

    // init
    public TxResultDispatcher(ChainClient chainClient) {
        executorService = Executors.newSingleThreadExecutor();
        this.nextBlockNum = -1;
        this.result = new ConcurrentHashMap<>();
        this.status = Status.START;
        this.chainClient = chainClient;
    }

    public void stop() {
        this.status = Status.STOP;
        executorService.shutdown();
    }

    public void register(String txId) {
        this.result.put(txId,  new Result());
    }


    public void unregister(String txId) {
        this.result.remove(txId);
    }

    /**
     * 根据交易id获取交易
     *
     * @param txId    交易id
     * @param timeout 超时时间
     * @return ChainmakerTransaction.Transaction
     */
    public  ChainmakerTransaction.Transaction getResult(String txId, long timeout) {

        long deadLine = System.currentTimeMillis() + timeout;
        ChainmakerTransaction.Transaction tx = null;
        while ((tx = result.get(txId).getTx()) == null && System.currentTimeMillis() < deadLine) {
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        if (tx == null) {
            logger.error("get txid {}  timeout...{}", txId, timeout);
        }
        return tx;
    }

    // 启动线程
    public void start() {
        try {
            ChainmakerBlock.BlockInfo block  = chainClient.getLastBlock(false, rpcCallTimeout);
            long blockHeight = block.getBlock().getHeader().getBlockHeight();
            if (blockHeight > 0) {
                nextBlockNum = blockHeight;
            }
            logger.info("subscribe get last height {} ", nextBlockNum);

        } catch (Exception e) {
            logger.warn("subscribe get last height {} ", e.getMessage());
        }
        executorService.execute(new AutoSubscribe());
    }

    //自动订阅任务类
    class AutoSubscribe implements Runnable {
        // 创建一个StreamObserver实例，用于处理订阅结果
        final StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult value) {
                // 当接收到新的区块信息时，解析区块数据并处理交易
                ChainmakerBlock.BlockInfo blockInfo;
                try {
                    blockInfo = ChainmakerBlock.BlockInfo.parseFrom(value.getData());
                } catch (InvalidProtocolBufferException e) {
                    throw new RuntimeException(e);
                }
                List<ChainmakerTransaction.Transaction> txs = blockInfo.getBlock().getTxsList();
                int size = txs.size();
                for (int i = 0; i < size; i++) {
                    ChainmakerTransaction.Transaction tx = txs.get(i);
                    String txId = tx.getPayload().getTxId();
                    if (result.containsKey(txId)) {
                        result.get(txId).setTx(tx);
                    }
                }
                // 更新下一个要订阅的区块高度
                nextBlockNum = blockInfo.getBlock().getHeader().getBlockHeight() + 1;
            }

            @Override
            public void onError(Throwable t) {
                // 当订阅过程中发生错误时，重新订阅上一个区块
                try {
                    Thread.sleep(100);
                    if (nextBlockNum > 0) {
                        nextBlockNum--;
                    }
                    chainClient.subscribeBlock(nextBlockNum, -1, true, false, responseObserver);
                } catch (Exception e) {
                    logger.error("=======subscribeBlock err:{}", e.getMessage());
                }
            }

            @Override
            public void onCompleted() {
                // 当订阅完成时，输出日志
                logger.info("==========onCompleted========");
            }
        };

        @Override
        public void run() {
            try {
                // 开始订阅区块
                chainClient.subscribeBlock(nextBlockNum, -1, true, false, responseObserver);
            } catch (Exception e) {
                logger.error("=======subscribeBlock err:{}", e.getMessage());
            }
        }
    }


    enum Status {
        START(0),

        STOP(1);

        private final Integer type;

        Status(int type) {
            this.type = type;
        }

        public int type() {
            return this.type;
        }
    }
}
