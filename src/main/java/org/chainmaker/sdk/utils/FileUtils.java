/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.utils;

import com.google.common.io.Resources;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 根据文件路径获取文件内容
     * 如果是在jar内访问，优先访问jar包外文件，然后在访问resource目录下文件
     * @param resourcePath 文件路径
     * @return byte数组
     * @throws UtilsException
     */
    public static byte[] getResourceFileBytes(String resourcePath) throws UtilsException {
        byte[] fileBytes = null;

        String protocol = Objects.requireNonNull(FileUtils.class.getResource("")).getProtocol();
        if (Objects.equals(protocol, "jar")) {
            try {
                fileBytes = getFileBytes(resourcePath);
                return fileBytes;
            } catch (Exception e) {
                logger.warn("jar get file fail, msg :{}", e.getMessage());
            }
            try {
                fileBytes = IOUtils.toByteArray(Objects.requireNonNull(FileUtils.class.getClassLoader().getResourceAsStream(resourcePath)));
                return fileBytes;
            } catch (Exception e) {
                logger.warn("jar two get file fail, msg :{}", e.getMessage());
            }
        }
        try {
            fileBytes = IOUtils.toByteArray((BufferedInputStream) Resources.getResource(resourcePath).getContent());
        } catch (IOException e) {
            throw new UtilsException("get file by path err : " + e.getMessage());
        }
        return fileBytes;

    }

    public static byte[] getFileOrResourceFileBytes(String path) throws UtilsException {
        try {
            return getFileBytes(path);
        } catch (UtilsException e) {
            logger.warn("get file fail, msg :{}", e.getMessage());
            return getResourceFileBytes(path);
        }
    }

    public static byte[] getFileBytes(String filePath) throws UtilsException {
        byte[] fileBytes = null;
        File f = null;
        try {
            f = new File(filePath);
            fileBytes = IOUtils.toByteArray(new FileInputStream(f));
        } catch (IOException e) {
            if (f != null) {
                System.out.println(f.getAbsolutePath());
            }
            throw new UtilsException("get file by path err : " + e.getMessage());
        }
        return fileBytes;
    }

    public static List<String> getFilesByPath(String path) {
        List<String> files = new ArrayList<>();
        File file = new File(path);
        File[] tempList = file.listFiles();
        if (tempList != null) {
            for (File value : tempList) {
                if (value.isFile() && value.getName().contains(".crt")) {
                    files.add(value.toString());
                }
            }
        }
        return files;
    }

    public static String getResourceFilePath(String resourcePath) {
        return System.getProperty("user.dir") + "/src/main/resources/" + resourcePath;
    }

    public static boolean isFileExist(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }
}
