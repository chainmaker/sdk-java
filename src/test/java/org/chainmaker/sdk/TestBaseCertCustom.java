/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.chainmaker.sdk.config.*;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.crypto.ChainmakerX509CryptoSuite;
import org.chainmaker.sdk.crypto.CustomSignInterface;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Before;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.*;

public class TestBaseCertCustom {

    static String ORG1_CERT_PATH = "crypto-config1/wx-org1.chainmaker.org/ca/ca.crt";
    static String CLIENT1_TLS_KEY_PATH = "crypto-config1/wx-org1.chainmaker.org/user/client1/client1.tls.key";
    static String CLIENT1_TLS_CERT_PATH = "crypto-config1/wx-org1.chainmaker.org/user/client1/client1.tls.crt";
    static String CLIENT1_KEY_PATH = "crypto-config1/wx-org1.chainmaker.org/user/client1/client1.sign.key";
    static String CLIENT1_CERT_PATH = "crypto-config1/wx-org1.chainmaker.org/user/client1/client1.sign.crt";
    static String TLS_HOST_NAME1 = "chainmaker.org";
    static int CONNECT_COUNT = 10;

    static String ADMIN1_TLS_KEY_PATH = "crypto-config1/wx-org1.chainmaker.org/user/admin1/admin1.tls.key";
    static String ADMIN1_TLS_CERT_PATH = "crypto-config1/wx-org1.chainmaker.org/user/admin1/admin1.tls.crt";
    static String ADMIN2_TLS_KEY_PATH = "crypto-config1/wx-org2.chainmaker.org/user/admin1/admin1.tls.key";
    static String ADMIN2_TLS_CERT_PATH = "crypto-config1/wx-org2.chainmaker.org/user/admin1/admin1.tls.crt";
    static String ADMIN3_TLS_KEY_PATH = "crypto-config1/wx-org3.chainmaker.org/user/admin1/admin1.tls.key";
    static String ADMIN3_TLS_CERT_PATH = "crypto-config1/wx-org3.chainmaker.org/user/admin1/admin1.tls.crt";

    static String ADMIN1_KEY_PATH = "crypto-config1/wx-org1.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN1_CERT_PATH = "crypto-config1/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt";
    static String ADMIN2_KEY_PATH = "crypto-config1/wx-org2.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN2_CERT_PATH = "crypto-config1/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt";
    static String ADMIN3_KEY_PATH = "crypto-config1/wx-org3.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN3_CERT_PATH = "crypto-config1/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt";

    static String ADMIN1_PK_PATH = "crypto-config1/node1/admin/admin1/admin1.key";
    static String ADMIN2_PK_PATH = "crypto-config1/node1/admin/admin2/admin2.key";
    static String ADMIN3_PK_PATH = "crypto-config1/node1/admin/admin3/admin3.key";

    static String ADMIN1_PWK_PATH = "crypto-config1/wx-org1.chainmaker.org/user/admin1/admin1.key";
    static String ADMIN2_PWK_PATH = "crypto-config1/wx-org2.chainmaker.org/user/admin1/admin1.key";
    static String ADMIN3_PWK_PATH = "crypto-config1/wx-org3.chainmaker.org/user/admin1/admin1.key";
    private static final String CHAIN_ID = "chain1";
    private static final int MAX_MESSAGE_SIZE = 16;

    static String CLIENT_CRL_PATH = "crl/client1.crl";

    static String ORG_ID1 = "wx-org1.chainmaker.org";
    static String ORG_ID2 = "wx-org2.chainmaker.org";
    static String ORG_ID3 = "wx-org3.chainmaker.org";

    static String NODE_GRPC_URL1 = "127.0.0.1:12301";

    static String TYPE = "mysql";
    static String DEST = "root:123456:localhost:3306";
    static String SECRET_KEY = "xxx";
    static String SDK_CONFIG = "sdk_config.yml";
    static String PUB_KEY = "crypto-config1/wx-org3.chainmaker.org/user/client1/client1.pem";

    ChainClient chainClient;
    ChainManager chainManager;
    User adminUser1;
    User adminUser2;
    User adminUser3;

    long rpcCallTimeout = 10000;
    long syncResultTimeout = 10000;


    // 自定义签名
    class HsmTest implements CustomSignInterface {

        @Override
        public void init() throws ChainMakerCryptoSuiteException {
            System.out.println("cert");
        }

        @Override
        public byte[] signWithCustom(byte[] plainText, Map params) throws ChainMakerCryptoSuiteException {
            if (plainText == null || plainText.length == 0) {
                throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
            }
            try {
                Signature sig = Signature.getInstance((String) params.get("algorithm"), BouncyCastleProvider.PROVIDER_NAME);
                sig.initSign((PrivateKey) params.get("private"));
                sig.update(plainText);
                return sig.sign();

            } catch (Exception e) {
                throw new ChainMakerCryptoSuiteException(e.toString());
            }
        }

        @Override
        public Map parseUserKey(byte[] userKeyBytes) throws ChainMakerCryptoSuiteException {
            Map params = new HashMap<>();
            params.put("private", CryptoUtils.getPrivateKeyFromBytes(userKeyBytes));
            return params;
        }

    }

    // 链基础的初始化配置
    @Before
    public void init() throws IOException, SdkException {
        ChainmakerX509CryptoSuite.setCryptoHsmInterface(new HsmTest());
        //通过sdk_config.yaml配置文件创建
        //如果不想通过配置文件设置参数，可自定义SdkConfig对象，设置SdkConfig中各个属性，参考initWithNoConfig
        Representer representer = new Representer(new DumperOptions());
        representer.getPropertyUtils().setSkipMissingProperties(true);
        Yaml yaml = new Yaml(representer);
        InputStream in = TestBaseCertCustom.class.getClassLoader().getResourceAsStream(SDK_CONFIG);

        SdkConfig sdkConfig;
        sdkConfig = yaml.loadAs(in, SdkConfig.class);
        assert in != null;
        in.close();

        for (NodeConfig nodeConfig : sdkConfig.getChainClient().getNodes()) {
            List<byte[]> tlsCaCertList = new ArrayList<>();
            if (nodeConfig.getTrustRootPaths() != null) {
                for (String rootPath : nodeConfig.getTrustRootPaths()) {
                    List<String> filePathList = FileUtils.getFilesByPath(rootPath);
                    for (String filePath : filePathList) {
                        tlsCaCertList.add(FileUtils.getFileBytes(filePath));
                    }
                }
            }
            byte[][] tlsCaCerts = new byte[tlsCaCertList.size()][];
            tlsCaCertList.toArray(tlsCaCerts);
            nodeConfig.setTrustRootBytes(tlsCaCerts);
        }

        chainManager = ChainManager.getInstance();
        chainClient = chainManager.getChainClient(sdkConfig.getChainClient().getChainId());

        if (chainClient == null) {
            chainClient = chainManager.createChainClient(sdkConfig);
        }

        adminUser1 = new User(ORG_ID1, FileUtils.getResourceFileBytes(ADMIN1_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH),
                FileUtils.getResourceFileBytes(ADMIN1_TLS_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN1_TLS_CERT_PATH), true);

        adminUser2 = new User(ORG_ID2, FileUtils.getResourceFileBytes(ADMIN2_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN2_CERT_PATH),
                FileUtils.getResourceFileBytes(ADMIN2_TLS_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN2_TLS_CERT_PATH), true);

        adminUser3 = new User(ORG_ID3, FileUtils.getResourceFileBytes(ADMIN3_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN3_CERT_PATH),
                FileUtils.getResourceFileBytes(ADMIN3_TLS_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN3_TLS_CERT_PATH), true);

    }

}

