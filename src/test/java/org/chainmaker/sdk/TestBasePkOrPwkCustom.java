/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.chainmaker.sdk.config.NodeConfig;
import org.chainmaker.sdk.config.SdkConfig;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.crypto.ChainmakerX509CryptoSuite;
import org.chainmaker.sdk.crypto.CustomSignInterface;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Before;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.*;

public class TestBasePkOrPwkCustom {

    static String ADMIN1_PK_PATH = "crypto-config/node1/admin/admin1/admin1.key";
    static String ADMIN2_PK_PATH = "crypto-config/node1/admin/admin2/admin2.key";
    static String ADMIN3_PK_PATH = "crypto-config/node1/admin/admin3/admin3.key";

    static String ADMIN1_PK_PEM_PATH = "crypto-config/node1/admin/admin1/admin1.pem";
    static String ADMIN2_PK_PEM_PATH = "crypto-config/node1/admin/admin2/admin2.pem";
    static String ADMIN3_PK_PEM_PATH = "crypto-config/node1/admin/admin3/admin3.pem";

    static String ADMIN1_PWK_PATH = "crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.key";
    static String ADMIN2_PWK_PATH = "crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.key";
    static String ADMIN3_PWK_PATH = "crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.key";
    private static final String CHAIN_ID = "chain1";
    private static final int MAX_MESSAGE_SIZE = 16;

    static String CLIENT_CRL_PATH = "crl/client1.crl";

    static String ORG_ID1 = "wx-org1.chainmaker.org";
    static String ORG_ID2 = "wx-org2.chainmaker.org";
    static String ORG_ID3 = "wx-org3.chainmaker.org";

    static String SDK_CONFIG = "sdk_config_pk.yml";

    ChainClient chainClient;
    ChainManager chainManager;
    User adminUser1;
    User adminUser2;
    User adminUser3;

    long rpcCallTimeout = 10000;
    long syncResultTimeout = 10000;

    // 自定义签名
    class HsmTest implements CustomSignInterface {

        @Override
        public void init() throws ChainMakerCryptoSuiteException {
            System.out.println("pk");
        }

        private static final String ALGORITHM_SM2_KEY = "SM3withSM2";
        private static final String ALGORITHM_RSA = "SHA256withRSA";
        private static final String ALGORITHM_ECDSA = "SHA256withECDSA";


        @Override
        public byte[] signWithCustom(byte[] plainText, Map params) throws ChainMakerCryptoSuiteException {
            if (plainText == null || plainText.length == 0) {
                throw new ChainMakerCryptoSuiteException("Data that to be signed is null.");
            }
            //NONEwithRSA
            try {
                Signature sig;
                String hash = (String) params.get("hash");
                PrivateKey privateKey = (PrivateKey) params.get("private");
                if (hash != null && hash.equals("SM3")) {
                    sig = Signature.getInstance(ALGORITHM_SM2_KEY, BouncyCastleProvider.PROVIDER_NAME);
                }else if (Objects.equals(privateKey.getAlgorithm(), "ECDSA")) {
                    sig = Signature.getInstance(ALGORITHM_ECDSA, BouncyCastleProvider.PROVIDER_NAME);
                } else {
                    sig = Signature.getInstance(ALGORITHM_RSA, BouncyCastleProvider.PROVIDER_NAME);
                }
                sig.initSign(privateKey);
                sig.update(plainText);
                return sig.sign();
            } catch (Exception e) {
                throw new ChainMakerCryptoSuiteException(e.toString());
            }
        }

        @Override
        public Map parseUserKey(byte[] userKeyBytes) throws ChainMakerCryptoSuiteException {
            Map params = new HashMap<>();
            params.put("hash", "SHA256");
            params.put("private", CryptoUtils.getPrivateKeyFromBytes(userKeyBytes));
            return params;
        }

    }

    // 链基础的初始化配置
    // 自定义签名需要传入公钥pem和椭圆曲线算法
    @Before
    public void init() throws IOException, SdkException {
        ChainmakerX509CryptoSuite.setCryptoHsmInterface(new HsmTest());
        //通过sdk_config.yaml配置文件创建
        //如果不想通过配置文件设置参数，可自定义SdkConfig对象，设置SdkConfig中各个属性，参考initWithNoConfig
        Representer representer = new Representer(new DumperOptions());
        representer.getPropertyUtils().setSkipMissingProperties(true);
        Yaml yaml = new Yaml(representer);
        InputStream in = TestBasePkOrPwkCustom.class.getClassLoader().getResourceAsStream(SDK_CONFIG);

        SdkConfig sdkConfig;
        sdkConfig = yaml.loadAs(in, SdkConfig.class);
        assert in != null;
        in.close();

        for (NodeConfig nodeConfig : sdkConfig.getChainClient().getNodes()) {
            List<byte[]> tlsCaCertList = new ArrayList<>();
            if (nodeConfig.getTrustRootPaths() != null) {
                for (String rootPath : nodeConfig.getTrustRootPaths()) {
                    List<String> filePathList = FileUtils.getFilesByPath(rootPath);
                    for (String filePath : filePathList) {
                        tlsCaCertList.add(FileUtils.getFileBytes(filePath));
                    }
                }
            }
            byte[][] tlsCaCerts = new byte[tlsCaCertList.size()][];
            tlsCaCertList.toArray(tlsCaCerts);
            nodeConfig.setTrustRootBytes(tlsCaCerts);
        }
        // 自定义签名需要传入公钥pem和椭圆曲线算法
        sdkConfig.getChainClient().setUserSignCrtFilePath("src/test/resources/crypto-config/node1/admin/admin1/admin1.pem");
        sdkConfig.getChainClient().getCrypto().setAlgo("EC");
        chainManager = ChainManager.getInstance();
        chainClient = chainManager.getChainClient(sdkConfig.getChainClient().getChainId());

        if (chainClient == null) {
            chainClient = chainManager.createChainClient(sdkConfig);
        }

        //公钥模式下，多签用户
//        adminUser1 = new User(ORG_ID1,
//                FileUtils.getResourceFileBytes(ADMIN1_PK_PATH),
//                FileUtils.getResourceFileBytes(ADMIN1_PK_PEM_PATH),
//                sdkConfig.getChainClient().getCrypto(),
//                true
//                );
//        adminUser1.setAuthType(AuthType.Public.getMsg());
//
//        adminUser2 = new User(ORG_ID2,
//                FileUtils.getResourceFileBytes(ADMIN2_PK_PATH),
//                FileUtils.getResourceFileBytes(ADMIN2_PK_PEM_PATH),
//                sdkConfig.getChainClient().getCrypto(),
//                true
//        );
//        adminUser2.setAuthType(AuthType.Public.getMsg());
//
//        adminUser3 = new User(ORG_ID3,
//                FileUtils.getResourceFileBytes(ADMIN3_PK_PATH),
//                FileUtils.getResourceFileBytes(ADMIN3_PK_PEM_PATH),
//                sdkConfig.getChainClient().getCrypto(),
//                true
//        );
//        adminUser3.setAuthType(AuthType.Public.getMsg());

//        //pwk 模式
//        adminUser1 = new User(ORG_ID1);
//        adminUser1.setAuthType(AuthType.PermissionedWithKey.getMsg());
//        adminUser1.setPriBytes(FileUtils.getResourceFileBytes(ADMIN1_PWK_PATH));
//
//        adminUser2 = new User(ORG_ID2);
//        adminUser2.setAuthType(AuthType.PermissionedWithKey.getMsg());
//        adminUser2.setPriBytes(FileUtils.getResourceFileBytes(ADMIN2_PWK_PATH));
//
//        adminUser3 = new User(ORG_ID3);
//        adminUser3.setAuthType(AuthType.PermissionedWithKey.getMsg());
//        adminUser3.setPriBytes(FileUtils.getResourceFileBytes(ADMIN3_PWK_PATH));

    }

}

