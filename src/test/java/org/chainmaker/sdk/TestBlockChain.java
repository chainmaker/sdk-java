/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk;

import org.chainmaker.pb.config.LocalConfig;
import org.chainmaker.sdk.config.NodeConfig;
import org.junit.Assert;
import org.junit.Test;

public class TestBlockChain extends TestBase {

    @Test
    public void testCheckNewBlockChainConfig() {
        LocalConfig.CheckNewBlockChainConfigResponse response = null;
        try {
            NodeConfig nodeConfig = new NodeConfig();
            nodeConfig.setNodeAddr("127.0.0.1:12301");
            nodeConfig.setEnableTls(true);
            nodeConfig.setTlsHostName("chainmaker.org");
            nodeConfig.setTrust_root_paths(new String[]{"src/test/resources/crypto-config/node1/ca/wx-org1.chainmaker.org/ca"});
            response = chainClient.checkNewBlockChainConfig(nodeConfig, rpcCallTimeout);
            System.out.println(response.getCode());
//            NodeConfig nodeConfig2 = new NodeConfig();
//            nodeConfig2.setNodeAddr("127.0.0.1:12302");
//            nodeConfig2.setEnableTls(true);
//            nodeConfig2.setTlsHostName("chainmaker.org");
//            nodeConfig2.setTrust_root_paths(new String[]{"src/test/resources/crypto-config/node2/ca"});
//            response =   chainClient.checkNewBlockChainConfig(nodeConfig2, rpcCallTimeout);
//            System.out.println(response.getCode());
//            System.out.println(response.getMessage());
        } catch (ChainClientException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(response);
    }

    @Test
    public void testGetChainMakerServerVersion() {
        String version = null;
        try {
            version =    chainClient.getChainMakerServerVersion(rpcCallTimeout);
        } catch (ChainClientException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(version);
        System.out.println("chainmaker version:" + version);
    }

    @Test
    public void testStop() {
        chainClient.stop();
    }
}
