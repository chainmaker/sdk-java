/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.sdk.utils.SdkUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// pk or pwk 自定义签名
//public class TestChainConfigCustom extends TestBasePkOrPwkCustom {
// cert 自定义签名
public class TestChainConfigCustom extends TestBaseCertCustom {

    private static final Logger logger = LoggerFactory.getLogger(TestChainConfigCustom.class);

    private final String ORG_ID = "wx-org1";
    private final String NODE_ID = "QmQVkTSF6aWzRSddT3rro6Ve33jhKpsHFaQoVxHKMWzhuN";

    @Test
    public void testGetChainConfig() {
        ChainConfigOuterClass.ChainConfig chainConfig = null;
        try {
            chainConfig = chainClient.getChainConfig(rpcCallTimeout);
            logger.info(chainConfig.getVersion());
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        System.out.println(chainConfig);
        Assert.assertNotNull(chainConfig.toString());
    }

    @Test
    public void testGetChainConfigByBlockHeight() {
        ChainConfigOuterClass.ChainConfig chainConfig = null;
        try {
            chainConfig = chainClient.getChainConfigByBlockHeight(3, rpcCallTimeout);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(chainConfig.toString());
    }


}
