/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.chainmaker.sdk;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.pb.common.ContractOuterClass.Contract;
import org.chainmaker.pb.common.Request;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.utils.*;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.utils.Numeric;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class TestDockerGoContract extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestDockerGoContract.class);

    private static final String GO_CONTRACT_FILE_PATH = "docker-fact.7z";
    private static final String CONTRACT_NAME = "fact";
    private static final String CONTRACT_ARGS_GO_PARAM = "data";
    private static String ADDRESS = "";

    @Test
    public void testCreateDockerGoContract() {

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            byte[] byteCode = FileUtils.getResourceFileBytes(GO_CONTRACT_FILE_PATH);
            // 1. create payload
            Request.Payload payload = chainClient.createContractCreatePayload(CONTRACT_NAME,
                    "1", byteCode,
                    ContractOuterClass.RuntimeType.DOCKER_GO, null);

            //2. create payloads with endorsement
            Request.EndorsementEntry[] endorsementEntries = SdkUtils.getEndorsers(
                    payload, new User[]{adminUser1, adminUser2, adminUser3});

            // 3. send request
            responseInfo = chainClient.sendContractManageRequest(
                    payload, endorsementEntries, rpcCallTimeout, syncResultTimeout);
            System.out.println(responseInfo);

            if (responseInfo.getCode() == ResultOuterClass.TxStatusCode.SUCCESS) {
                Contract contract = Contract.newBuilder().mergeFrom(responseInfo.getContractResult().getResult().toByteArray()).build();
                String jsonStr = JsonFormat.printer().print(contract);
                System.out.println(jsonStr);
            }
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testInvokeDockerGoContract() throws UtilsException, ChainClientException, ChainMakerCryptoSuiteException {
        Map<String, byte[]> params = new HashMap<>();
        params.put("method", "save".getBytes());
        params.put("time", "time-test".getBytes());
        params.put("file_hash", "hash-test".getBytes());

        ResultOuterClass.TxResponse responseInfo = null;
        for (int i = 0; i < 10000; i++) {
            try {
                // 一般而言查询类请求应该使用：queryContract；query不出块而invoke出块。
                // 此处为了展示出块交易结果和合约结果解析和入参的校验一致，特意写invokeContract
                responseInfo = chainClient.invokeContract(CONTRACT_NAME,
                        "invoke_contract", null, params, rpcCallTimeout, syncResultTimeout);
            } catch (SdkException e) {
                logger.info("invoke faild");
//                e.printStackTrace();
//                Assert.fail(e.getMessage());
            }
            logger.info(responseInfo.getMessage());
        }

        //查询本次交易结果
        ChainmakerTransaction.TransactionInfo tx = chainClient.getTxByTxId(responseInfo.getTxId(), rpcCallTimeout);
        logger.info("调用合约结果：{},查询交易合约执行结果:{}", Numeric.toBigInt(responseInfo.getContractResult().getResult().toByteArray()),
                tx.getTransaction());
    }
}
