/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk;

import org.chainmaker.pb.common.Request;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.chainmaker.sdk.utils.SdkUtils;
import org.chainmaker.sdk.utils.UtilsException;
import org.junit.Assert;
import org.junit.Test;

public class TestPayerManage extends TestBase {

    //1.启动区块链网络(需要开启gas)
//        ./prepare.sh 4 1
//        ./build_release.sh
//        ./cluster_quick_start.sh normal

    //2.配置证书/私钥,启用gas
//        mv ../tools/cmc/testdata/crypto-config ../tools/cmc/testdata/crypto-config-bak
//        cp -rf ../build/crypto-config ../tools/cmc/testdata/crypto-config
//        cd ../tools/cmc
//        mv ../../../chainmaker-sdk-java/src/test/resources/crypto-config ../../../chainmaker-sdk-java/src/test/resources/crypto-config-bak
//        cp -rf testdata/crypto-config ../../../chainmaker-sdk-java/src/test/resources/

//        ./cmc client gas \
//        --sdk-conf-path=./testdata/sdk_config.yml \
//        --admin-key-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key \
//        --admin-crt-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt \
//        --gas-enable=true
//        ./cmc gas set-admin \
//        --sdk-conf-path=./testdata/sdk_config.yml \
//        --admin-key-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key \
//        --admin-crt-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt
//        ./cmc gas recharge \
//        --sdk-conf-path=./testdata/sdk_config.yml \
//        --address=4f91097f2873ff1e8971d3a67dd08bcba0a691d8 \   #上一步admin地址
//        --amount=10000000000000000

    //3.（使用admin1）安装合约
//        ./cmc client contract user create \
//        --contract-name=fact \
//        --runtime-type=WASMER \
//        --byte-code-path=./testdata/claim-wasm-demo/rust-fact-2.0.0.wasm \
//        --version=1.0 \
//        --sdk-conf-path=./testdata/sdk_config.yml \
//        --admin-key-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key \
//        --admin-crt-file-paths=./testdata/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt,./testdata/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt \
//        --sync-result=true  --enable-cert-hash=false \
//        --gas-limit=600000 \
//        --params="{}"

    @Test
    public void testSetContractMethodPayer(){
        //4. 设置payer
        String hashType = "SHA256";
        String algo = "EC";
        String certAddr = CryptoUtils.getEVMAddressFromCertBytes(adminUser2.getCertBytes());

        System.out.printf("addr=%s\n", certAddr);

        String payerAddress = certAddr;
        String contractName = "fact";
        String methodName = "save";
        String requestId  = "";
        String payerOrgId = ORG_ID2;
        byte[] payerKeyPem = new byte[0];
        try {
            payerKeyPem = FileUtils.getResourceFileBytes(ADMIN2_KEY_PATH);
        } catch (UtilsException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        byte[] payerCertPem = adminUser2.getCertBytes();

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            Request.Payload payload = chainClient.createSetContractMethodPayerPayload(
                payerAddress, contractName,methodName,requestId, payerOrgId,
                payerKeyPem, payerCertPem
            );
            Request.EndorsementEntry[] endorsementEntries = SdkUtils
                    .getEndorsers(payload, new User[]{adminUser1, adminUser2, adminUser3});

            responseInfo = chainClient.sendRequestWithSync(payload, endorsementEntries, rpcCallTimeout, true);

        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);

        System.out.println(responseInfo);
    }

    //5.查询payer
//    ./cmc gas query-method-payer fact save --sdk-conf-path=./testdata/sdk_config.yml

    @Test
    public void testUnsetContractMethodPayer(){

        //6. 取消设置payer
        String contractName = "fact";
        String methodName = "save";

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            Request.Payload payload = chainClient.createUnsetContractMethodPayerPayload(
                    contractName,methodName
            );
            Request.EndorsementEntry[] endorsementEntries = SdkUtils
                    .getEndorsers(payload, new User[]{adminUser1, adminUser2, adminUser3});

            responseInfo = chainClient.sendRequestWithSync(payload, endorsementEntries, rpcCallTimeout, true);

        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);

        System.out.println(responseInfo);
    }
    //7.查询payer
//    ./cmc gas query-method-payer fact save --sdk-conf-path=./testdata/sdk_config.yml

    @Test
    public void testQueryContractMethodPayer(){

        //8. 查询payer
        String contractName = "fact";
        String methodName = "save";

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            Request.Payload payload = chainClient.createQueryContractMethodPayerPayload(
                    contractName,methodName
            );

            responseInfo = chainClient.sendRequestWithSync(payload, null, rpcCallTimeout, true);

        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);

        System.out.println(responseInfo);
    }
    @Test
    public void testQueryTxPayer(){

//    调用合约
//    ./cmc client contract user invoke --contract-name=fact --method=save --sdk-conf-path=./testdata/sdk_config.yml --params="{\"file_name\":\"name007\",\"file_hash\":\"ab3456df5799b87c77e7f88\",\"time\":\"6543234\"}" --payer-org-id="wx-org2.chainmaker.org" --payer-crt-file-path=./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt --payer-key-file-path=./testdata/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key --gas-limit=600000 --sync-result=true

        //9. 查询Txpayer
        String txId = "17e250533c6d1440ca738cebf56307c21973157bfc5449c2b617f64b02a55d0f";

        ResultOuterClass.TxResponse responseInfo = null;
        try {
            Request.Payload payload = chainClient.createQueryTxPayerPayload(
                    txId
            );

            responseInfo = chainClient.sendRequestWithSync(payload, null, rpcCallTimeout, true);

        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);

        System.out.println(responseInfo);
    }
}

