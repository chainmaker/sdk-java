/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ResultOuterClass;
import org.junit.Assert;
import org.junit.Test;

public class TestSubscribe extends TestBase {

    private static final boolean ONLY_HEADER = false;

    // 从数据库获取高度
    static Long lastProcessedBlockHeight = 3400L;

    /**
     * 订阅重试示例
     */
    @Test
    public void testSubscribeContractEvent() throws Exception {
        new Thread(new TestSubscribe.AutoSubscribe()).start();// 启动一个线程执行订阅
        Thread.sleep(1000 * 1000);// 保证主线程不退出
    }

    @Test
    public void testSubscribeBlock() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    if (ONLY_HEADER) {
                        ChainmakerBlock.BlockHeader blockHeader = ChainmakerBlock.BlockHeader.parseFrom(result.getData());
                        Assert.assertNotNull(blockHeader);
                        System.out.println("###new block-header:");
                        System.out.println("height:" + blockHeader.getBlockHeight());
                        System.out.println("tx-count:" + blockHeader.getTxCount());
                    } else {
                        ChainmakerBlock.BlockInfo blockInfo = ChainmakerBlock.BlockInfo.parseFrom(result.getData());
                        Assert.assertNotNull(blockInfo);
                        System.out.println("###new block:");
                        System.out.println("height:" + blockInfo.getBlock().getHeader().getBlockHeight());
                        System.out.println("tx-count:" + blockInfo.getBlock().getTxsCount());
                    }
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // just do nothing
            }

            @Override
            public void onCompleted() {
                // just do nothing
            }
        };
        try {
            chainClient.subscribeBlock(0, -1, true, ONLY_HEADER, responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSubscribeTx() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    ChainmakerTransaction.Transaction transactionInfo = ChainmakerTransaction.Transaction.parseFrom(result.getData());
                    Assert.assertNotNull(transactionInfo);
                    System.out.println("###new tx:");
                    System.out.println(transactionInfo.getPayload().getTxId());
                    System.out.println(transactionInfo.getPayload().getContractName());
                    System.out.println(transactionInfo.getPayload().getMethod());
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // can add log here
            }

            @Override
            public void onCompleted() {
                // can add log here
            }
        };
        try {
            chainClient.subscribeTx(0, -1, "", new String[]{}, responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSubscribeTxByPreAlias() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    ChainmakerTransaction.Transaction transactionInfo = ChainmakerTransaction.Transaction.parseFrom(result.getData());
                    Assert.assertNotNull(transactionInfo);
                    System.out.println("###new tx:");
                    System.out.println(transactionInfo.getPayload().getTxId());
                    System.out.println(transactionInfo.getPayload().getContractName());
                    System.out.println(transactionInfo.getPayload().getMethod());
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // can add log here
            }

            @Override
            public void onCompleted() {
                // can add log here
            }
        };
        try {
            chainClient.subscribeTxByPreAlias(0, -1, "001", responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSubscribeTxByPreTxId() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    ChainmakerTransaction.Transaction transactionInfo = ChainmakerTransaction.Transaction.parseFrom(result.getData());
                    Assert.assertNotNull(transactionInfo);
                    System.out.println("###new tx:");
                    System.out.println(transactionInfo.getPayload().getTxId());
                    System.out.println(transactionInfo.getPayload().getContractName());
                    System.out.println(transactionInfo.getPayload().getMethod());
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // can add log here
            }

            @Override
            public void onCompleted() {
                // can add log here
            }
        };
        try {
            chainClient.subscribeTxByPreTxId(0, -1, "17c662", responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSubscribeTxByPreOrgId() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    ChainmakerTransaction.Transaction transactionInfo = ChainmakerTransaction.Transaction.parseFrom(result.getData());
                    Assert.assertNotNull(transactionInfo);
                    System.out.println("###new tx:");
                    System.out.println(transactionInfo.getPayload().getTxId());
                    System.out.println(transactionInfo.getPayload().getContractName());
                    System.out.println(transactionInfo.getPayload().getMethod());
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // can add log here
            }

            @Override
            public void onCompleted() {
                // can add log here
            }
        };
        try {
            chainClient.subscribeTxByPreOrgId(0, -1, "wx-org1", responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSubscribeContractEvent2() {
        StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                try {
                    ResultOuterClass.ContractEventInfoList contract = ResultOuterClass.ContractEventInfoList.parseFrom(result.getData());
                    Assert.assertNotNull(contract);
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // can add log here
            }

            @Override
            public void onCompleted() {
                // can add log here
            }
        };
        try {
            chainClient.subscribeContractEvent(0, -1, "topic_vx", "asset", responseObserver);
            Thread.sleep(500000000);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }



    class AutoSubscribe implements Runnable {
        // 创建一个StreamObserver实例，用于处理订阅结果
        final StreamObserver<ResultOuterClass.SubscribeResult> responseObserver = new StreamObserver<ResultOuterClass.SubscribeResult>() {
            @Override
            public void onNext(ResultOuterClass.SubscribeResult result) {
                // 接收到订阅消息处理逻辑
                try {
                    ResultOuterClass.ContractEventInfoList contract = ResultOuterClass.ContractEventInfoList.parseFrom(result.getData());
                    Long blockHeight = contract.getContractEvents(0).getBlockHeight();
                    // 打印接收订阅日志
                    System.out.println("receive blockHeight：" + blockHeight);

                    // 1. 校验blockHeight区块高度是否已处理
                    if (lastProcessedBlockHeight >= blockHeight) {
                        return;
                    }

                    // 2. 处理业务
                    System.out.println("process blockHeight：" + blockHeight);

                    // 3. 更新 lastProcessedBlockHeight 标记【落库记录】
                    lastProcessedBlockHeight = blockHeight;

                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("onError 订阅区块错误"+t.getMessage());
                while (true) {
                    // 开始订阅区块
                    try {
                        // 错误时等待重试，必须等待，否则可能出现一次性订阅多个的情况
                        Thread.sleep(1000 * 10);
                        System.out.println("onError 订阅区块重试开始" + lastProcessedBlockHeight);
                        chainClient.subscribeContractEvent(lastProcessedBlockHeight, -1, "", "dtdh_fee_v1", responseObserver);
                        System.out.println("onError 订阅区块成功" + lastProcessedBlockHeight);
                        break; // 订阅成功退出循环
                    } catch (Exception e) {
                        System.out.println("onError 订阅区块失败"+e.getMessage());
                    }
                }
            }

            @Override
            public void onCompleted() {
                // 当订阅完成时，输出日志
                System.out.println("onCompleted 订阅区块结束");
            }
        };

        @Override
        public void run() {
            while (true) {
                // 开始订阅区块
                try {
                    System.out.println("run 订阅区块开始" + lastProcessedBlockHeight);
                    chainClient.subscribeContractEvent(lastProcessedBlockHeight, -1, "", "dtdh_fee_v1", responseObserver);
                    System.out.println("run 订阅区块成功" + lastProcessedBlockHeight);
                    break; // 订阅成功退出循环
                } catch (Exception e) {
                    System.out.println("run 订阅区块失败"+e.getMessage());
                }

                // 错误则重试
                System.out.println("run 订阅区块重试" + lastProcessedBlockHeight);
                try {
                    // 错误时等待重试，必须等待，否则可能出现一次性订阅多个的情况
                    Thread.sleep(1000 * 10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
