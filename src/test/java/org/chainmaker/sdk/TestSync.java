package org.chainmaker.sdk;

import org.chainmaker.pb.sync.Sync;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.Test;

public class TestSync extends TestBase {
    private static final Logger logger = LoggerFactory.getLogger(TestSync.class);


    @Test
    public void testGetSyncState() {
        try {
            Sync.SyncState syncState = chainClient.getSyncState(true, rpcCallTimeout);
            logger.debug("区块同步状态开始:====================");
            logger.debug("{}", syncState);
            logger.debug("区块同步状态结束:====================");
            Assert.assertNotNull(syncState);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}